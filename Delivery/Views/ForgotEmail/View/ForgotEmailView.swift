//
//  ForgotEmailView.swift
//  EazzyEats
//
//  Created by Bit Mini on 11/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class ForgotEmailView: BaseTableViewController {
    
    @IBOutlet weak var view_Mobile: UIView!
    @IBOutlet weak var textField_CountryCode: CustomTextField!
    @IBOutlet weak var textField_MobileNumber: CustomTextField!
    @IBOutlet var countryCodeImage: UIImageView!
    @IBOutlet weak var btn_Submit: NormalBoldButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = .white
        self.navigationController?.isNavigationBarHidden = false
        textField_CountryCode.delegate = self
        countryCodeImage.image = UIImage(named: SingleToneClass.shared.defaultPhoneCountry?.code?.lowercased() ?? "")
            textField_CountryCode.text = SingleToneClass.shared.defaultPhoneCountry?.dialCode
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view_Mobile.setRoundedCornered(height: view_Mobile.frame.size.height)
        self.btn_Submit.setRoundedCornered(height: btn_Submit.frame.size.height)
    }
    
    @IBAction func verifyButtonAction(_ sender: Any) {
        
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "verifyOTPView") as! VerifyOTPView
        self.navigationController?.pushViewController(redViewController, animated: true)
    }
    @IBAction func btn_BackdidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        
        if !(Utility.isValidMobileNumber(textField_MobileNumber.text ?? "" )) {
            showAlert(message: "Please provide valide mobile number")
        }
        else {
            changeEmail()
        }
        
    }
    
   func changeEmail() {
         let fetchAllUserRepository = ChangeEmailRepository()
    AMShimmer.start(for: textField_MobileNumber)
    AMShimmer.start(for: btn_Submit)
    fetchAllUserRepository.updateEmail(vc: self,phone : textField_MobileNumber.text ?? "" ) { (response, success, error) in
                    DispatchQueue.main.async {
                        AMShimmer.stop(for: self.textField_MobileNumber)
                        AMShimmer.stop(for: self.btn_Submit)
                    }
                       if (error != nil) {
                           Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                       }
                       if (success) {
                           if response.statuscode == 200 {
                            
                           let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "verifyOTPView") as! VerifyOTPView
                            redViewController.email = response.responseData?.email ?? ""
                            self.navigationController?.pushViewController(redViewController, animated: true)
                            }else if response.statuscode == 422 {
                                Utility.showAlert(withMessage: response.message ?? "", onController: self)
                           }else {
                                Utility.showAlert(withMessage: response.message ?? "", onController: self)
                           }
                       }else{
                           Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                       }
                   }
    }
}

// MARK: - Table view data source
extension ForgotEmailView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 160, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        case 3:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 150, currentScrenHeight: self.view.bounds.height)
        case 4:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
        default:
            return 0.0
        }
    }
}

extension ForgotEmailView : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textField_CountryCode {
            let storyboard = UIStoryboard(name: "CountryPicker", bundle: nil)
            let countryModalVC = storyboard.instantiateViewController(identifier: "CountryPickerViewController") as! CountryPickerViewController
            countryModalVC.delegate = self
            self.navigationController?.present(countryModalVC, animated: true, completion: nil)
        }
    }
}
extension ForgotEmailView : CountryPickerViewControllerDelegate {
    func didSelectCountry(country: Country) {
        
        print("country : ", country.code?.lowercased() as Any )
        
        countryCodeImage.image = UIImage(named: country.code?.lowercased() ?? "")
        textField_CountryCode.text = country.dialCode
        SingleToneClass.shared.defaultPhoneCountry = country
    }
}
