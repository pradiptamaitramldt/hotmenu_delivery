//
//  NewDeliveryView.swift
//  EazzyEatsDelivery
//
//  Created by Ivica Technologies on 14/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class NewDeliveryView: UIViewController {
    
    @IBOutlet weak var Sidemenubutton: UIBarButtonItem!
    @IBOutlet var headerLabel: CustomBoldLabel!
    @IBOutlet weak var orderDetailesButton: NormalBoldButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.orderDetailesButton.isHidden = true
        
        /*
        let label = UILabel()
        label.backgroundColor = .clear
        label.numberOfLines = 1
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .left
        label.textColor = .white
        label.text = "Pretoria street,kolkata..."
        self.navigationItem.titleView = label
        
        if let navigationBar = self.navigationController?.navigationBar {
            let firstFrame = CGRect(x: 100, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
            let secondFrame = CGRect(x: 300, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
            
            //navigationBar.frame.width/5
            let firstLabel = UILabel(frame: firstFrame)
            firstLabel.text = ""
            
            let secondLabel = UILabel(frame: secondFrame)
            secondLabel.text = "Change"
            secondLabel.font = UIFont.boldSystemFont(ofSize: 8)
         
            navigationBar.addSubview(firstLabel)
            navigationBar.addSubview(secondLabel)
        }
        */
        
       // let button = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action:#selector(self.clickButton))
       // self.navigationItem.rightBarButtonItem  = button
        
      // Sidemenubutton.target = revealViewController()
        
     //  Sidemenubutton.action = #selector(SWRevealViewController.revealToggle(_:))
        // Sidemenubutton.tintColor = UIColor(displayP3Red: 7/255.0 , green: 173/255.0, blue: 237/255.0, alpha: 1.0)
      //  self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @objc func clickButton(sender: AnyObject){
        let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "NotificationListVc") as! NotificationListVc
        redViewController.fromVC = "NewDelivery"
        self.navigationController?.pushViewController(redViewController, animated: true)
    }
    
    @IBAction func newBeliveryButton(_ sender: UIButton) {
        
    }
    
}

//MARK:- IBAction

extension NewDeliveryView {
    
    @IBAction func contactResturentButton(_ sender: Any) {
        let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
        let customePopUpViews = mainStoryBoard.instantiateViewController(withIdentifier: "CustomePopUpViews") as! CustomePopUpViews
        customePopUpViews.modalTransitionStyle = .crossDissolve
        customePopUpViews.modalPresentationStyle = .overCurrentContext
        self.present(customePopUpViews, animated: true, completion: nil)
    }
    
    @IBAction func trackRouteButton(_ sender: Any) {
        let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
        let trackingVC = mainStoryBoard.instantiateViewController(withIdentifier: "TrackingVC") as! TrackingVC
        self.navigationController?.pushViewController(trackingVC, animated: true)
    }
    
    @IBAction func collectedButton(_ sender: Any) {
        let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
        let customePopUpViews = mainStoryBoard.instantiateViewController(withIdentifier: "CustomePopUpViews") as! CustomePopUpViews
        customePopUpViews.isCameFrom = "pickUpView"
        customePopUpViews.modalTransitionStyle = .crossDissolve
        customePopUpViews.modalPresentationStyle = .overCurrentContext
        self.present(customePopUpViews, animated: true, completion: nil)
    }
    
}
