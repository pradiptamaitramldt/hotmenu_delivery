//
//  ResetEmailIDController.swift
//  Delivery
//
//  Created by BrainiumSSD on 11/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit
import AMShimmer

class ResetEmailIDController: UIViewController {

    @IBOutlet var submitButton: NormalBoldButton!
    @IBOutlet var emailIDText: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        if !(Utility.isValidEmail(emailIDText.text ?? "" )) {
            showAlert(message: "Please provide valid emailID")
        } else {
            resetEmail()
        }
    }
    func resetEmail() {
        
        let fetchAllUserRepository = ChangeEmailRepository()
        AMShimmer.start(for: submitButton)
        AMShimmer.start(for: emailIDText)
        fetchAllUserRepository.resetEmail(vc: self,email : emailIDText.text ?? "" ) { (response, success, error) in
            DispatchQueue.main.async {
                           AMShimmer.stop(for: self.submitButton)
                           AMShimmer.stop(for: self.emailIDText)
            }
                        if (error != nil) {
                            Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                        }
                        if (success) {
                            if response.statuscode == 200 {
                                Utility.showAlert(withMessage: response.message!, onController: self)
                             }else if response.statuscode == 422 {
                                Utility.showAlert(withMessage: response.message ?? "", onController: self)
                            }else {
                                 Utility.showAlert(withMessage: response.message ?? "", onController: self)
                            }
                        }else{
                            Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                        }
                    }
     }
}
