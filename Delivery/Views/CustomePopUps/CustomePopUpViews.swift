//
//  CustomePopUpViews.swift
//  EazzyEatsDelivery
//
//  Created by Ivica Technologies on 14/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class CustomePopUpViews: UIViewController {
    
    @IBOutlet weak var showView: UIView!
    @IBOutlet weak var pickUpview: UIView!
    @IBOutlet var checkButton: UIButton!
    
    
    var isCameFrom: String = "",isChecked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isCameFrom == "pickUpView" {
            pickUpview.isHidden = false
            showView.isHidden = true
        } else {
            pickUpview.isHidden = true
            showView.isHidden = false
        }
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func checkButtonAction(_ sender: Any) {
        isChecked = !isChecked
        if isChecked {
            checkButton.setImage(UIImage(named: "selected_checkbox"), for: .normal)
        }
        else {
            checkButton.setImage(UIImage(named: "checkbox"), for: .normal)
        }
        
    }
    @IBAction func confirmButtonAction(_ sender: Any) {
        
        if isChecked {
             self.dismiss(animated: false, completion: nil)
        }
        else {
            showAlert(message: "Please check the box to confirm the pickup")
        }
        
    }
    
    
}
