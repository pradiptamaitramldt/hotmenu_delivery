//
//  CommonResponseModel.swift
//  EazzyEats
//
//  Created by Bit Mini on 13/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation
// MARK: - CommonResponseModel
class CommonResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: ResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: ResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class CommonResponseData: Codable {

    init() {
    }
}
