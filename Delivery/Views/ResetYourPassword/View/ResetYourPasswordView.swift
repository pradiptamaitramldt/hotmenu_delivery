//
//  ResetYourPasswordView.swift
//  EazzyEats
//
//  Created by Bit Mini on 11/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class ResetYourPasswordView: BaseTableViewController {
    
    @IBOutlet weak var view_NewPassword: UIView!
    @IBOutlet weak var textFieldNewPassword: CustomTextField!
    @IBOutlet weak var view_ConfirmPassword: UIView!
    @IBOutlet weak var textFieldConfirmPassword: CustomTextField!
    @IBOutlet weak var btn_Submit: NormalBoldButton!
    
    var presenter = ResetPasswordPresenter()
    var email : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = .white
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = .white
    }
   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view_NewPassword.setRoundedCornered(height: view_NewPassword.frame.size.height)
        self.view_ConfirmPassword.setRoundedCornered(height: view_ConfirmPassword.frame.size.height)
        self.btn_Submit.setRoundedCornered(height: btn_Submit.frame.size.height)
    }
    
    @IBAction func btn_BackdidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_SubmitDidClick(_ sender: Any) {
//        self.startLoaderAnimation()
//        self.presenter.resetPassword(email: email, password: self.textFieldNewPassword.text!, confirmPassword: self.textFieldConfirmPassword.text!) { (status, message) in
//            self.stopLoaderAnimation()
//            DispatchQueue.main.async {
//                self.showAlertWith(message: message) {
//                    if status{
//                        self.navigationController?.popToRootViewController(animated: true)
//                    }
//                }
//            }
//        }
   //     if Utility.isValidEmail(<#T##email: String##String#>)
        if !(Utility.isValidText(textFieldNewPassword.text ?? "")) {
            showAlert(message: "Please provide password")
        }
        else if !(Utility.isValidPassword(textFieldNewPassword.text ?? "")) {
            showAlert(message: "Please provide valid password")
        }
      else  if !(Utility.isValidText(textFieldConfirmPassword.text ?? "")) {
                   showAlert(message: "Please provide  confirem password")
               }
        else if  (textFieldConfirmPassword.text ?? "" != textFieldNewPassword.text ?? "") {
             showAlert(message: "Password and confirm password does not match")
        }
        else  {
            resetPassword()
        }
    }
    func resetPassword() {
        let fetchAllUserRepository = ChangePasswordRepository()
        let param = ["email": email,
                     "password" : textFieldNewPassword.text!,
                     "confirmPassword" : textFieldConfirmPassword.text!]
        AMShimmer.start(for: textFieldNewPassword)
        AMShimmer.start(for: textFieldConfirmPassword)
         AMShimmer.start(for: btn_Submit)
        fetchAllUserRepository.resetPassword(parameter : param, vc: self ) { (response, success, error) in
                      DispatchQueue.main.async {
                          AMShimmer.stop(for: self.textFieldNewPassword)
                          AMShimmer.stop(for: self.textFieldConfirmPassword)
                          AMShimmer.stop(for: self.btn_Submit)
                      }
                        if (error != nil) {
                            Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                        }
                        if (success) {
                            if response.statuscode == 200 {
                                self.navigationController?.popToRootViewController(animated: true)
                             }else if response.statuscode == 422 {
                                 Utility.showAlert(withMessage: response.message ?? "", onController: self)
                            }else {
                                 Utility.showAlert(withMessage: response.message ?? "", onController: self)
                            }
                        }else{
                            Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                        }
                    }
     }
}

// MARK: - Table view data source
extension ResetYourPasswordView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 43, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        case 3:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 148, currentScrenHeight: self.view.bounds.height)
        default:
            return 0.0
        }
    }
}
