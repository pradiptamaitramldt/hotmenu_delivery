//
//  OrderDetailesView.swift
//  EazzyEatsDelivery
//
//  Created by Ivica Technologies on 13/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import SDWebImage
import AMShimmer

class OrderDetailesView: UIViewController {

    @IBOutlet weak var orderDetailesTableview: UITableView!
    @IBOutlet var checkButton: UIButton!
    @IBOutlet var checkingView: UIView!
    @IBOutlet var orderIDLabel: CustomBoldLabel!
    @IBOutlet var orderNOLabel: CustomBoldLabel!
    @IBOutlet var customerNameLabel: CustomBoldLabel!
    @IBOutlet var customerAddressLabel: CustomBoldLabel!
    @IBOutlet var restaurantNameLabel: CustomBoldLabel!
    @IBOutlet var confirmButton: NormalBoldButton!
    @IBOutlet var statusLabel: CustomNormalLabel!
    @IBOutlet var labelType: CustomNormalLabel!
    
    var isChecked = false,orderDetailsType = "",orderID  = ""
    var orderDetailsData : OrderDetailsModel?
    var utility = Utility()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.orderDetailesTableview.register(UINib.init(nibName: "OrderDetailesCell", bundle: nil), forCellReuseIdentifier: "OrderDetailesCell")
//        self.orderDetailesTableview.register(UINib.init(nibName: "OrderDetailsSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderDetailsSummaryTableViewCell")
        self.orderDetailesTableview.dataSource = self
        self.orderDetailesTableview.delegate = self
        self.orderDetailesTableview.backgroundColor = .white
        
        /*
        let label = UILabel()
        label.backgroundColor = .clear
        label.numberOfLines = 1
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .left
        label.textColor = .white
        label.text = "Pretoria street,kolkata..."
        self.navigationItem.titleView = label
        
        if let navigationBar = self.navigationController?.navigationBar {
            let firstFrame = CGRect(x: 100, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
            let secondFrame = CGRect(x: 300, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
         
            //navigationBar.frame.width/5
            let firstLabel = UILabel(frame: firstFrame)
            firstLabel.text = ""
            
            let secondLabel = UILabel(frame: secondFrame)
            secondLabel.text = "Change"
            secondLabel.font = UIFont.boldSystemFont(ofSize: 8)
            navigationBar.addSubview(firstLabel)
            navigationBar.addSubview(secondLabel)
        }
         Customer Contract Number:
        */

     //   let button = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action:#selector(self.clickButton))
    //    self.navigationItem.rightBarButtonItem  = button
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
           print("orderDetailsType",orderDetailsType)
           fetchOrderDetailsData()
       }
           func fetchOrderDetailsData(){
               let fetchAllUserRepository = AllOrderRepository()
              AMShimmer.start(for: orderDetailesTableview)
               
               fetchAllUserRepository.getOrderDetails(vc:  self, orderID:  orderID) { (response, success, error) in
                 //  self.stopLoading()
                   DispatchQueue.main.async {
                                          AMShimmer.stop(for: self.orderDetailesTableview)
                                       }
                   if (error != nil) {
                      Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                   }
                   if (success) {
                       if response.statuscode == 200 {
                            if response.success == false {
                               Utility.showAlert(withMessage: response.message, onController: self)
                           }
                           else {
                               self.orderDetailsData = response
                            self.orderIDLabel.text = "Order ID : " +  ( self.orderDetailsData?.responseData.id ?? "" )
                            self.orderNOLabel.text = self.orderDetailsData?.responseData.orderID.orderNo
                            self.customerNameLabel.text = self.orderDetailsData?.responseData.customerID.fullName ?? ""
                            self.customerAddressLabel.text = self.orderDetailsData?.responseData.deliveryAddressID.fullAddress
                            if self.orderDetailsType == "Completed" {
                                self.checkingView.isHidden = true
                                self.confirmButton.isHidden = true
                                self.statusLabel.text = "Items deliver by you"
                                self.labelType.text = "Customer Contract Number: "
                                self.restaurantNameLabel.text = ( self.orderDetailsData?.responseData.customerID.countryCode  ?? "+91 " ) + String(self.orderDetailsData?.responseData.customerID.phone ?? 000000)
                                
                            }
                            else {
                                if self.orderDetailsType == "Pickup" {
                                    self.checkingView.isHidden = true
                                    self.confirmButton.isHidden = false
                                    self.confirmButton.setTitle("Complete Delivery", for: .normal)
                                    self.statusLabel.text = "Items to be deliver"
                                    self.labelType.text = "Customer Contract Number: "
                                    self.restaurantNameLabel.text = ( self.orderDetailsData?.responseData.customerID.countryCode  ?? "+91 " ) + String(self.orderDetailsData?.responseData.customerID.phone ?? 000000)

                                }
                                else {
                                    self.checkingView.isHidden = false
                                    self.confirmButton.isHidden = false
                                    self.confirmButton.setTitle("Confirm Pickup", for: .normal)
                                    self.statusLabel.text = "Items to be picked"
                                    self.labelType.text = "Vendor Contract Number: "
                                   self.restaurantNameLabel.text = "+91 " + String(self.orderDetailsData?.responseData.vendorID.contactPhone ?? 000000)

                                }
                            }
                                self.orderDetailesTableview.reloadData()
                           }
                           
                       }else if response.statuscode == 422 {
                           Utility.showAlert(withMessage: (response.message), onController: self)
                       }else {
                           Utility.showAlert(withMessage: (response.message), onController: self)
                       }
                   }else{
                       Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                   }
               }
           }
    @objc func clickButton(sender: AnyObject) {
           
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
     }
    @IBAction func checkButtonAction(_ sender: Any) {
        isChecked = !isChecked
        if isChecked {
            checkButton.setImage(UIImage(named: "selected_checkbox"), for: .normal)
        }
        else {
            checkButton.setImage(UIImage(named: "checkbox"), for: .normal)
        }
    }
    
    @IBAction func getRouteButtonAction(_ sender: Any) {
        
        let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
                      let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "TrackingVC") as! TrackingVC
        redViewController.orderDetailsType = orderDetailsType
        redViewController.orderDetailsData  = self.orderDetailsData
                      self.navigationController?.pushViewController(redViewController, animated: true)
    }
    
    @IBAction func confirmPickupButtonAction(_ sender: Any) {
        if orderDetailsType == "Pickup" {
            pickupOrderDeliver(status: 3)
        }
        else {
            if isChecked{
                pickupOrderDeliver(status: 2)
            }
            else {
                showAlert(message: "Please select the check box")
            }
        }
        
    }
    func pickupOrderDeliver(status : Int){
         let userID = self.utility.Retrive(Constants.Strings.UserID) as! String
        let parameter = ["restaurantId" : orderDetailsData?.responseData.vendorID.id ?? "",
                         "orderId" : orderDetailsData?.responseData.orderID.id ?? "",
                         "changeStatus" : status,
                         "deliveryBoyId" : userID] as [String : Any]
        
        let fetchAllUserRepository = AllOrderRepository()
        AMShimmer.start(for: confirmButton)
        fetchAllUserRepository.changeOrderStatus(vc:  self,params : parameter) { (response, success, error) in
          //  self.stopLoading()
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.confirmButton)
            }
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    //self.showAlert(message: response.message)
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: HomeDeliveryView.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }else if response.statuscode == 422 {
                    Utility.showAlert(withMessage: (response.message), onController: self)
                }else {
                    Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    
}

//MARK:- TableView DataSource & Delegates
extension OrderDetailesView:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (orderDetailsData?.responseData.orderID.cartID.menus.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailesCell") as! OrderDetailesCell
        cell.itemName.text = (orderDetailsData?.responseData.orderID.cartID.menus[indexPath.row].menuID.itemID.itemName)
        cell.itemImage.sd_setImage(with: URL(string:(orderDetailsData?.responseData.orderID.cartID.menus[indexPath.row].menuID.itemID.menuImage ?? "") ), placeholderImage: UIImage(named: "image-2"))
        cell.itemQurnatityLabel.text =  String(orderDetailsData?.responseData.orderID.cartID.menus[indexPath.row].menuQuantity ?? 0) + " items"
            return cell
        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        switch indexPath.section {
//        case 0:
//            return UITableView.automaticDimension
//        default:
//            return tableView.bounds.size.width * (5 / 16)
//        }
//
//    }
    
}
