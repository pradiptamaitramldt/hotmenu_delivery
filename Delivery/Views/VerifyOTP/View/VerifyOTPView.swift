//
//  VerifyOTPView.swift
//  EazzyEats
//
//  Created by Bit Mini on 11/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class VerifyOTPView: BaseTableViewController {
    
    @IBOutlet var emailView: UIView!
    @IBOutlet weak var view_OTP: UIView!
    @IBOutlet weak var textFieldOTP: CustomTextField!
    @IBOutlet weak var btn_ResendCode: NormalBoldButton!
    @IBOutlet weak var btn_SubmitCode: NormalBoldButton!
    @IBOutlet var showEmailIDLabel: CustomNormalLabel!
    @IBOutlet var newEmailIDText: CustomTextField!
    
    var email : String = ""
    var presenter = VerifyOTPPresenter()
    var verifyOTPCommand: VerifyOTPCommand?
    var utility = Utility()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.tableView.backgroundColor = .white
        self.navigationItem.backBarButtonItem?.tintColor = .white
        self.showEmailIDLabel.text = "Code has been sent to your  Email ID : \(email)"
        self.newEmailIDText.text = email
        self.verifyOTPCommand = VerifyOTPCommand(email: nil, OTP: nil)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        emailView.setRoundedCornered(height : emailView.frame.size.height)
        self.view_OTP.setRoundedCornered(height: view_OTP.frame.size.height)
        self.btn_ResendCode.setRoundedCornered(height: btn_ResendCode.frame.size.height)
        self.btn_SubmitCode.setRoundedCornered(height: btn_SubmitCode.frame.size.height)
    }

    @IBAction func btn_BackdidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_ResendCodeClick(_ sender: Any) {
      //  self.startLoaderAnimation()
        self.presenter.resendOTP(email: self.email) { (statsu, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.showAlertWith(message: message) {
                    
                }
            }
        }
    }
    
    @IBAction func btn_SubmitDidClick(_ sender: Any) {

        self.updateCommand()
        self.verifyOTPCommand?.validate(completion: { (isValid, message) in
            if(isValid){
                guard let changePasswordCommand = self.verifyOTPCommand else{
                    return
                }
                self.changePassword(changePasswordCommand: changePasswordCommand)
            }else{
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
    }
    private func updateCommand() {
              self.verifyOTPCommand?.email = self.newEmailIDText.text
              self.verifyOTPCommand?.OTP = self.textFieldOTP.text
           
          }
    func changePassword(changePasswordCommand: VerifyOTPCommand?) {
        
        let changePasswordRepository = VerifyOTPRepository()
        AMShimmer.start(for: newEmailIDText)
        AMShimmer.start(for: textFieldOTP)
         AMShimmer.start(for: btn_SubmitCode)
        changePasswordRepository.verifyOTP(vc: self,otpCommand : verifyOTPCommand! ) { (response, success, error) in
          DispatchQueue.main.async {
              AMShimmer.stop(for: self.newEmailIDText)
              AMShimmer.stop(for: self.textFieldOTP)
              AMShimmer.stop(for: self.btn_SubmitCode)
          }
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {

                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "resetYourPasswordView") as! ResetYourPasswordView
                    redViewController.email = self.newEmailIDText.text ?? ""
                self.navigationController?.pushViewController(redViewController, animated: true)
                 }else if response.statuscode == 422 {
                    Utility.showAlert(withMessage: response.message , onController: self)
                }else {
                    Utility.showAlert(withMessage: response.message , onController: self)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
}

// MARK: - Table view data source

extension VerifyOTPView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 150, currentScrenHeight: self.view.bounds.height)
        case 3:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 73, currentScrenHeight: self.view.bounds.height)
        case 4:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 24, currentScrenHeight: self.view.bounds.height)
        case 5:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 113, currentScrenHeight: self.view.bounds.height)
        case 6:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
        default:
            return 0.0
        }
    }
}
