//
//  LoginWithEmailPresenter.swift
//  EazzyEats
//
//  Created by Bit Mini on 13/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class LoginWithEmailPresenter: NSObject {
let deviceId : String = UIDevice.current.identifierForVendor!.uuidString
    var utility = Utility()
    func loginUser(email : String, password : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        
        if Utility.isValidEmail(email){
            if Utility.isValidPassword(password){
                let param : [String: Any] = ["user" : email,
                                             "password"  : password,
                                             "loginType" : "GENERAL",
                                             "deviceToken": deviceId,
                                             "appType" : "IOS",
                                             "pushMode" : "P",
                                             "userType" : "DELIVERY_BOY"]
                WebServiceManager.shared.requestAPI(url: WebServiceConstants.loginUser, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: param, httpMethodType: .post) { (data, err) in
                    if let responseData = data{
                        let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: RegistraionResponseModel.self) { (resData, err) in
                            if resData != nil{
                                if resData?.statuscode! == ResponseCode.success.rawValue{
                                    
                                    self.utility.Save( (resData?.responseData?.userDetails?.id) ?? "NA", keyname: Constants.Strings.UserID)
                                    self.utility.Save((resData?.responseData?.userDetails?.email!)!, keyname: Constants.Strings.UserEmail)
                                    self.utility.Save((resData?.responseData?.userDetails?.loginID!)!, keyname: Constants.Strings.LoginID)
                                    self.utility.Save((resData?.responseData?.authToken)!, keyname: Constants.Strings.Token)
                                    self.utility.Save(resData?.responseData?.userDetails?.profileImage ?? "", keyname: Constants.Strings.profileImage)
                                    let fName = (resData?.responseData?.userDetails?.firstName ?? "")
                                    let lName = (resData?.responseData?.userDetails?.lastName ?? "")
                                    let fullName = fName + " " + lName
                                    self.utility.Save(fullName, keyname: Constants.Strings.userFullName)
                                     NotificationCenter.default.post(name: Notification.Name("UpdateSliderProfileValue"), object: nil)
                                    completion(true,"")
                                }else{
                                    completion(false,(resData?.message!)!)
                                }
                            }else{
                                completion(false, err!.localizedDescription)
                            }
                        }
                    }
                }
            }else{
                completion(false, "Please add a suitable password.")
            }
        }else{
            completion(false, "Please enter Your Email.")
        }
        
    }

}
