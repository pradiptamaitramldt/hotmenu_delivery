//
//  LoginWithEmailView.swift
//  EazzyEats
//
//  Created by Bit Mini on 07/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class LoginWithEmailView: BaseTableViewController {
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var textFieldEmail: CustomTextField!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var textFieldPassword: CustomTextField!
    @IBOutlet weak var button_Login: NormalBoldButton!
    
    var presenter = LoginWithEmailPresenter()
    var utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = .white
        self.navigationController?.isNavigationBarHidden = true
        
        let userID: String = self.utility.Retrive(Constants.Strings.UserID) as! String
        print("userID\(userID)")
        if !userID.isEmpty {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeDeliveryView") as! HomeDeliveryView
            self.navigationController?.pushViewController(redViewController, animated: true)
            
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewEmail.setRoundedCornered(height: viewEmail.frame.size.height)
        self.viewPassword.setRoundedCornered(height: viewPassword.frame.size.height)
        self.button_Login.setRoundedCornered(height: button_Login.frame.size.height)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    @IBAction func btn_LoginDidClick(_ sender: Any) {
        
       // self.startLoaderAnimation()
        AMShimmer.start(for: textFieldEmail)
        AMShimmer.start(for: textFieldPassword)
        AMShimmer.start(for: button_Login)
        self.presenter.loginUser(email: self.textFieldEmail.text!, password: self.textFieldPassword.text!) { (status, message) in
          //  self.stopLoaderAnimation()
            
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.textFieldEmail)
                                   AMShimmer.stop(for: self.textFieldPassword)
                                   AMShimmer.stop(for: self.button_Login)
                if status{
                   
                    self.textFieldEmail.text = ""
                    self.textFieldPassword.text = ""
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeDeliveryView") as! HomeDeliveryView
                    self.navigationController?.pushViewController(redViewController, animated: true)
                }
                else{
                    //self.navigationController?.popToRootViewController(animated: true)
                    self.showAlertWith(message: message) {
                    }
                }
                
            }
        }
    }
    
    
    @IBAction func btn_BackdidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension LoginWithEmailView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 220, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 90, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 29, currentScrenHeight: self.view.bounds.height)
        case 3:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 90, currentScrenHeight: self.view.bounds.height)
        case 4:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 145, currentScrenHeight: self.view.bounds.height)
        case 5:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 200, currentScrenHeight: self.view.bounds.height)
        default:
            return 0.0
        }
    }
}
