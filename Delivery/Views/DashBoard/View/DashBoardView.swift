//
//  DashBoardView.swift
//  EazzyEats
//
//  Created by Bit Mini on 11/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class DashBoardView: BaseViewController {

    @IBOutlet weak var viewBottom: UIView!
    
    var bottom_menu = BottomMenu()

    override func viewDidLoad() {
        super.viewDidLoad()
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.viewBottom.bounds.size.width,height:self.viewBottom.bounds.size.height)
        self.viewBottom.addSubview(bottom_menu)
        bottom_menu.delegate = self

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: bottomMenu delegate
extension DashBoardView: bottomMenuDelegate{
    func selectedButton(selectedItem:NSInteger){
        
        if selectedItem == 1 {
            //home
//            let st = UIStoryboard.init(name: "Main", bundle: nil)
//            let controller = st.instantiateViewController(withIdentifier:"HomeVC") as! HomeVC
//            self.navigationController?.pushViewController(controller, animated: true)
        }else if selectedItem == 2{
            //inventory
//            let inventory_count = webService_obj.Retrive("inventory_count")
//
//            if inventory_count as! String == "0" {
//                let st = UIStoryboard.init(name: "Inventory", bundle: nil)
//                let controller = st.instantiateViewController(withIdentifier:"AddDeckVC") as! AddDeckVC
//                self.navigationController?.pushViewController(controller, animated: true)
//            }else{
//                let st = UIStoryboard.init(name: "Inventory", bundle: nil)
//                let controller = st.instantiateViewController(withIdentifier:"InventoryVC") as! InventoryVC
//                self.navigationController?.pushViewController(controller, animated: true)
//            }
        }
        else if selectedItem == 3{
            //service
//            let st = UIStoryboard.init(name: "Services", bundle: nil)
//            let controller = st.instantiateViewController(withIdentifier:"ServiceFeatureVC") as! ServiceFeatureVC
//            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            //product
//            let st = UIStoryboard.init(name: "Product", bundle: nil)
//            let controller = st.instantiateViewController(withIdentifier:"ProductsVC") as! ProductsVC
//            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
}
