//
//  ChangePasswordController.swift
//  Delivery
//
//  Created by BrainiumSSD on 11/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit
import AMShimmer

class ChangePasswordController: UIViewController {

    @IBOutlet var oldPasswordText: CustomTextField!
    @IBOutlet var newPasswordText: CustomTextField!
    @IBOutlet var confirmPasswordText: CustomTextField!
    @IBOutlet var submitButton: NormalBoldButton!
    
    var changePasswordCommand: ChangePasswordCommand?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.changePasswordCommand = ChangePasswordCommand.init(oldPassword: nil, newPassword: nil, confirmPassword: nil)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        
        self.updateCommand()
        self.changePasswordCommand?.validate(completion: { (isValid, message) in
            if(isValid){
                guard let changePasswordCommand = self.changePasswordCommand else{
                    return
                }
                self.changePassword(changePasswordCommand: changePasswordCommand)
            }else{
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
    }
    private func updateCommand() {
           self.changePasswordCommand?.oldPassword = self.oldPasswordText.text
           self.changePasswordCommand?.newPassword = self.newPasswordText.text
           self.changePasswordCommand?.confirmPassword = self.confirmPasswordText.text
       }
    func changePassword(changePasswordCommand: ChangePasswordCommand?) {
        
        let changePasswordRepository = ChangePasswordRepository()
        AMShimmer.start(for: oldPasswordText)
        AMShimmer.start(for: newPasswordText)
        AMShimmer.start(for: confirmPasswordText)
        AMShimmer.start(for: submitButton)
        changePasswordRepository.changePassword(changePasswordCommand: changePasswordCommand!, vc: self) { (response, success, error) in
           DispatchQueue.main.async {
            AMShimmer.stop(for: self.oldPasswordText)
             AMShimmer.stop(for: self.newPasswordText)
             AMShimmer.stop(for: self.confirmPasswordText)
             AMShimmer.stop(for: self.submitButton)
            }
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode==200 {
                    Utility.showAlert(withMessage: response.message!, onController: self)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
}
