//
//  HomeDeliveryView.swift
//  EazzyEatsDelivery
//
//  Created by Ivica Technologies on 13/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import Alamofire
import AMShimmer
import CoreLocation

class HomeDeliveryView: BaseViewController {
    
    @IBOutlet weak var homeDeliveryTableview: UITableView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet var statusLabel: CustomBoldLabel!
   
    var assignOrderData  : [OrderListData] = []
    var pickupOrderData : [OrderListData] = []
    var deliveryOrderData : [OrderListData] = []
    var locationManager: CLLocationManager!
    var currentLocation: CLLocation?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var sidemenuBar: UIBarButtonItem!
    @IBOutlet var switchButton: UISwitch!
    @IBOutlet var labelName: CustomNormalLabel!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var assignView: UIView!
    @IBOutlet var pickupView: UIView!
    @IBOutlet var completeView: UIView!
    
    var orderListData : OrderListModel?
    var selectedTag = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        /*
         let label = UILabel()
         label.backgroundColor = .clear
         label.numberOfLines = 1
         label.font = UIFont.boldSystemFont(ofSize: 16.0)
         label.textAlignment = .left
         label.textColor = .white
         label.text = "Pretoria Street"
         self.navigationItem.titleView = label
         
         
         if let navigationBar = self.navigationController?.navigationBar {
         let firstFrame = CGRect(x: 100, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
         let secondFrame = CGRect(x: 300, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
         
         
         //navigationBar.frame.width/5
         let firstLabel = UILabel(frame: firstFrame)
         firstLabel.text = ""
         
         let secondLabel = UILabel(frame: secondFrame)
         secondLabel.text = "Change"
         secondLabel.font = UIFont.boldSystemFont(ofSize: 8)
         
         
         navigationBar.addSubview(firstLabel)
         navigationBar.addSubview(secondLabel)
         }
         
        */
        
        //        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        //        titleLabel.text = "  Home"
        //        titleLabel.textColor = UIColor.white
        //        titleLabel.font = UIFont.systemFont(ofSize: 20)
        //        navigationItem.titleView = titleLabel
        //
        
        
       // let button1 = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action:#selector(self.clickButton)) // action:#selector(Class.MethodName) for swift 3
      //  self.navigationItem.rightBarButtonItem  = button1
        
        backView.setCirculer()
        profileImage.setCirculer()
        
        homeDeliveryTableview.register(UINib.init(nibName: "HomeDeliveryCell", bundle: nil), forCellReuseIdentifier: "HomeDeliveryCell")
        homeDeliveryTableview.dataSource = self
        homeDeliveryTableview.delegate = self
        homeDeliveryTableview.backgroundColor = .white
        assignView.backgroundColor = UIColor(named: "ThemeColor")
        pickupView.backgroundColor = .white
        completeView.backgroundColor = .white
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        switchButton.isOn = false
         fetchProfileData()
        fetchAllDeliveriesData()
    }
    func fetchProfileData() {
        let fetchAllUserRepository = FetchProfileRepository()
        AMShimmer.start(for: profileImage)
        AMShimmer.start(for: labelName)
        fetchAllUserRepository.fetchProfile(vc:  self) { (response, success, error) in
            DispatchQueue.main.async {
               AMShimmer.stop(for: self.profileImage)
                 AMShimmer.stop(for: self.labelName)
            }
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    
                    print("data",response.responseData)
                    self.labelName.text = response.responseData.firstName + " " + response.responseData.lastName
                    self.profileImage.sd_setImage(with: URL(string: response.responseData.profileImage ), placeholderImage: UIImage(named: "dummyImage"))
                    if response.responseData.isActive {
                        self.switchButton.isOn = true
                    }
                    else {
                        self.switchButton.isOn = false
                    }
                }else if response.statuscode == 422 {
                    
                }else {
                    
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func fetchAllDeliveriesData(){
        let fetchAllUserRepository = AllOrderRepository()
         AMShimmer.start(for: homeDeliveryTableview)
        fetchAllUserRepository.getAllOrderList(vc:  self,lat : currentLocation?.coordinate.latitude ?? 22.587231,long: currentLocation?.coordinate.longitude ?? 88.371742) { (response, success, error) in
          DispatchQueue.main.async {
                                                AMShimmer.stop(for: self.homeDeliveryTableview)
                                             }
            
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    
                    print("data",response.responseData)
                    if response.responseData.count == 0 {
                        self.statusLabel.isHidden = false
                        self.homeDeliveryTableview.isHidden = true
                        Utility.showAlert(withMessage: response.message, onController: self)
                    }
                    else {
                        self.orderListData = response
                        self.statusLabel.isHidden = true
                        self.homeDeliveryTableview.isHidden = false
                        self.assignOrderData.removeAll()
                        self.pickupOrderData.removeAll()
                        self.deliveryOrderData.removeAll()
                        for index in 0...((response.responseData.count)  - 1) {
                            
                            if response.responseData[index].deliveryStatus == 1 {
                                self.assignOrderData.append(response.responseData[index])
                            }
                            else if response.responseData[index].deliveryStatus == 2 {
                                self.pickupOrderData.append(response.responseData[index])
                            }
                            else if response.responseData[index].deliveryStatus == 3 {
                                self.deliveryOrderData.append(response.responseData[index])
                            }
                        }
                        self.homeDeliveryTableview.reloadData()
                    }
                    
                }else if response.statuscode == 422 {
                    Utility.showAlert(withMessage: (response.message), onController: self)
                }else {
                   Utility.showAlert(withMessage: (response.message), onController: self)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    @objc func clickButton(sender: AnyObject){
        
        let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "NotificationListVc") as! NotificationListVc
        redViewController.fromVC = "HomeDelivery"
        self.navigationController?.pushViewController(redViewController, animated: true)
        
    }
    
    @IBAction func sideMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    @objc func didTapVideoButton(sender: AnyObject){
    }
    
    @objc func didTapSearchButton(sender: AnyObject){
    }
    
    @IBAction func switchAction(_ sender: Any) {
        if switchButton.isOn {
            print("on")
            changeActivityStatus(status: "YES")
        }
        else {
            print("off")
            changeActivityStatus(status: "NO")
        }
    }
    func changeActivityStatus(status : String) {
        let fetchAllUserRepository = ChangeActivityRepository()
        let params = ["activityStatus" : status]
        fetchAllUserRepository.changeAcitivityStatus(vc:  self,parameter:params) { (response, success, error) in
            //  self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    
                   
                    }
               else if response.statuscode == 422 {
                    Utility.showAlert(withMessage: (response.message), onController: self)
                }else {
                    
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
    }
}
    @IBAction func pickupDeliveres(_ sender: Any) {
        
        pickupView.backgroundColor = UIColor(named: "ThemeColor")
        completeView.backgroundColor = .white
        assignView.backgroundColor = .white
        selectedTag = 2
        if pickupOrderData.count == 0 {
            statusLabel.isHidden = false
        }
        else {
            statusLabel.isHidden = true
        }
        homeDeliveryTableview.reloadData()
        
    }
    
    @IBAction func assignOrderButtonAction(_ sender: Any) {
        assignView.backgroundColor = UIColor(named: "ThemeColor")
        pickupView.backgroundColor = .white
        completeView.backgroundColor = .white
        selectedTag = 1
        if assignOrderData.count == 0 {
            statusLabel.isHidden = false
        }
        else {
            statusLabel.isHidden = true
        }
        homeDeliveryTableview.reloadData()
    }
    
    @IBAction func completedDelivery(_ sender: Any) {
        
        selectedTag = 3
        completeView.backgroundColor = UIColor(named: "ThemeColor")
        pickupView.backgroundColor = .white
        assignView.backgroundColor = .white
        if deliveryOrderData.count == 0 {
            statusLabel.isHidden = false
        }
        else {
            statusLabel.isHidden = true
        }
        homeDeliveryTableview.reloadData()
    }
    
}
extension HomeDeliveryView : CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        currentLocation = location
        locationManager.stopUpdatingLocation()
        // listLikelyPlaces()
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        @unknown default:
            fatalError()
        }
    }
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
}
//MARK:- TABLEVIEW DELAGATES
extension HomeDeliveryView:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedTag == 1 {
            return assignOrderData.count
        }
        else if selectedTag == 2 {
            return pickupOrderData.count
        }
        else {
            return deliveryOrderData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDeliveryCell") as! HomeDeliveryCell
        //        cell.acceptButton.tag = indexPath.row
        //        cell.acceptButton.addTarget(self, action: #selector(accept(_:)), for: .touchUpInside)
        
        switch selectedTag {
        case 1:
            cell.restaurantNameLabel.text =  "You have  " + String(assignOrderData[indexPath.row].orderID.cartID.menus.count) + " order"
            cell.orderCountLabel.text = assignOrderData[indexPath.row].deliveryAddressID.fullAddress
            
            break
        case 2:
            
            cell.restaurantNameLabel.text =  "You have  " + String(pickupOrderData[indexPath.row].orderID.cartID.menus.count) + " order"
            cell.orderCountLabel.text = pickupOrderData[indexPath.row].deliveryAddressID.fullAddress
            
            break
        case 3:
    
            cell.restaurantNameLabel.text =  "You have  " + String(deliveryOrderData[indexPath.row].orderID.cartID.menus.count) + " order"
            cell.orderCountLabel.text = deliveryOrderData[indexPath.row].deliveryAddressID.fullAddress
        default:
            break
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    @objc func accept(_ sender:UIButton){
        
        let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "NewDeliveryView") as! NewDeliveryView
        //self.present(redViewController, animated: true, completion: nil)
        //  redViewController.orderDetailsType = "Assign"
        self.navigationController?.pushViewController(redViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
         let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "OrderDetailesView") as! OrderDetailesView
                //self.present(redViewController, animated: true, completion: nil)
                redViewController.orderDetailData = self.orderDetailsData
                redViewController.orderDetailsType = self.orderDetailsType
                self.navigationController?.pushViewController(redViewController, animated: true)
         */
        switch selectedTag {
        case 1:
            
            let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "OrderDetailesView") as! OrderDetailesView
            redViewController.orderID = assignOrderData[indexPath.row].id
            redViewController.orderDetailsType = "Assign"
            self.navigationController?.pushViewController(redViewController, animated: true)
            break
        case 2:
            let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "OrderDetailesView") as! OrderDetailesView
            redViewController.orderID = pickupOrderData[indexPath.row].id
            redViewController.orderDetailsType = "Pickup"
            self.navigationController?.pushViewController(redViewController, animated: true)
            
            break
        case 3:
            let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "OrderDetailesView") as! OrderDetailesView
            redViewController.orderID = deliveryOrderData[indexPath.row].id
            redViewController.orderDetailsType = "Completed"
            self.navigationController?.pushViewController(redViewController, animated: true)
            break
            
        default:
            break
        }
        
    }
    
}
