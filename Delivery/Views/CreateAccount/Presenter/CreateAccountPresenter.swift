//
//  CreateAccountPresenter.swift
//  EazzyEats
//
//  Created by Bit Mini on 12/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit


class CreateAccountPresenter: NSObject {

    var arrStates : [CountriesModel] = []
    var arrCities : [CountriesModel] = []
    var selectedStateID : String = ""
    var selectedCountryID : String = ""
    var isAllowedMail : Bool = false
    
    func getAllStates(completion : @escaping(_ data : Any ,_ success : Bool ,_ message : String) -> Void){
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getAllStates, httpHeader: nil, httpMethodType: .get) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetAllCountriesModel.self) { (resData, err) in
                    if resData != nil{
                        self.arrStates.removeAll()
                        self.arrStates = (resData?.responseData)!
                        completion(responseData,true,"")
                    }else{
                        completion(responseData,false, err!.localizedDescription)
                    }
                }
            }
        }
    }
    
    func getAllCities(stateID : String, completion : @escaping(_ data : Any ,_ success : Bool ,_ message : String) -> Void){
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getAllCities, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: ["stateId" : stateID], httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetAllCountriesModel.self) { (resData, err) in
                    if resData != nil{
                        self.arrCities.removeAll()
                        self.arrCities = (resData?.responseData)!
                        
                        completion(responseData,true,"")
                    }else{
                        completion(responseData,false, err!.localizedDescription)
                    }
                }
            }
        }

//        let json : [String: Any] = ["stateId" : stateID]
//        let jsonData: NSData
//
//        do {
//            jsonData = try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions()) as NSData
//            WebServiceManager.shared.requestAPI(url: WebServiceConstants.getAllCities, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: jsonData as Data, httpMethodType: .post) { (data, err) in
//                if let responseData = data{
//                    let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetAllCountriesModel.self) { (resData, err) in
//                        if resData != nil{
//                            self.arrCities.removeAll()
//                            self.arrCities = (resData?.responseData)!
//
//                            completion(responseData,true,"")
//                        }else{
//                            completion(responseData,false, err!.localizedDescription)
//                        }
//                    }
//                }
//            }
//        } catch _ {
//            print ("JSON Failure")
//
//        }
    }
    
    func createAccount(firstName : String, lastName : String, email : String, countryCode : String, phone : String, cityId : String, location : String, password : String,confirmPassword : String, promoCode : String,allowMail : Bool, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        
        if firstName.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            if lastName.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                if Utility.isValidEmail(email){
                    if Utility.isValidMobileNumber(phone){
                        if countryCode.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                            if Utility.isValidPassword(password){
                                if cityId.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                    if password == confirmPassword{
                                        let param : [String: Any] = ["firstName" : firstName,
                                                                     "lastName"  : lastName,
                                                                     "email"     : email,
                                                                     "phone"     : phone,
                                                                     "countryCode": countryCode,
                                                                     "cityId"     : cityId,
                                                                     "location"   : location,
                                                                     "password"   : password,
                                                                     "confirmPassword": confirmPassword,
                                                                     "allowMail"  : allowMail,
                                                                     "promoCode"  : promoCode]
                                        WebServiceManager.shared.requestAPI(url: WebServiceConstants.registerUser, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: param, httpMethodType: .post) { (data, err) in
                                            if let responseData = data{
                                                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: RegistraionResponseModel.self) { (resData, err) in
                                                    if resData != nil{
                                                        if resData?.statuscode! == ResponseCode.success.rawValue{
                                                            
                                                            completion(true,"Registration success. Please login with your credentials.")
                                                        }else{
                                                            completion(false,(resData?.message!)!)
                                                        }
                                                    }else{
                                                        completion(false, err!.localizedDescription)
                                                    }
                                                }
                                            }
                                        }
                                    }else{
                                        completion(false, "Confirm Password does'nt match.")
                                    }
                                }else{
                                    completion(false, "Please selct city.")
                                }
                            }else{
                                completion(false, "Please add a suitable password.")
                            }
                        }else{
                            completion(false, "Please add your Country Code.")
                        }
                    }else{
                        completion(false, "Please enter Your Mobile No.")
                    }
                }else{
                    completion(false, "Please enter Your Email.")
                }
            }else{
                completion(false, "Please enter Your Last Name.")
            }
        }else{
            completion(false, "Please enter Your First Name.")
        }
        
    }
}
