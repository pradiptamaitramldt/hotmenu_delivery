//
//  RegistraionResponseModel.swift
//  EazzyEats
//
//  Created by Bit Mini on 13/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

// MARK: - RegistraionResponseModel
class RegistraionResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: RegistraionModel?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: RegistraionModel?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class RegistraionModel: Codable {
    let userDetails: UserDetails?
    let authToken: String?

    init(userDetails: UserDetails?, authToken: String?) {
        self.userDetails = userDetails
        self.authToken = authToken
    }
}

// MARK: - UserDetails
class UserDetails: Codable {
    let firstName, lastName, email: String?
    let phone: String?
    let cityID, location, profileImage, id: String?
    let loginID : String?
    enum CodingKeys: String, CodingKey {
        case firstName, lastName, email, phone
        case cityID = "countryCode"
        case location, profileImage, id
        case loginID = "loginId"
    }

    init(firstName: String?, lastName: String?, email: String?, phone: String?, cityID: String?, location: String?, profileImage: String?, id: String?,loginID : String?) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phone = phone
        self.cityID = cityID
        self.location = location
        self.profileImage = profileImage
        self.id = id
        self.loginID = loginID
    }
}
