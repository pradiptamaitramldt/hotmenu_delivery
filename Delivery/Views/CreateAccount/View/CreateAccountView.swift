//
//  CreateAccountView.swift
//  EazzyEats
//
//  Created by Bit Mini on 10/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class CreateAccountView: UITableViewController {
    
    @IBOutlet weak var viewFName: UIView!
    @IBOutlet weak var textFieldFName: CustomTextField!
    @IBOutlet weak var viewLName: UIView!
    @IBOutlet weak var textFieldLName: CustomTextField!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var textFieldEmail: CustomTextField!
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var textFieldCountryCode: CustomTextField!
    @IBOutlet weak var textFieldMobile: CustomTextField!
    @IBOutlet weak var viewCountry: UIView!
    @IBOutlet weak var textFieldCountry: CustomTextField!
    @IBOutlet weak var imageCountryFlag: UIImageView!
    @IBOutlet weak var viewState: UIView!
    @IBOutlet weak var textFieldState: CustomTextField!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var textFieldLocation: CustomTextField!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewConfirmPassword: UIView!
    @IBOutlet weak var textPassword: CustomTextField!
    @IBOutlet weak var textConfirmPassword: CustomTextField!
    @IBOutlet weak var viewPromoCode: UIView!
    @IBOutlet weak var textFieldPromoCode: CustomTextField!
    @IBOutlet weak var btn_CheckBox: UIButton!
    @IBOutlet weak var btn_Confirm: NormalBoldButton!
    
    var presenter = CreateAccountPresenter()
    var statePicker : UIPickerView?
    var cityPicker : UIPickerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = .white
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.textFieldCountryCode.delegate = self
        self.viewFName.setRoundedCornered(height: viewFName.frame.size.height)
        self.viewLName.setRoundedCornered(height: viewLName.frame.size.height)
        self.viewEmail.setRoundedCornered(height: viewEmail.frame.size.height)
        self.viewMobile.setRoundedCornered(height: viewMobile.frame.size.height)
        self.viewCountry.setRoundedCornered(height: viewCountry.frame.size.height)
        self.viewState.setRoundedCornered(height: viewState.frame.size.height)
        self.viewLocation.setRoundedCornered(height: viewLocation.frame.size.height)
        self.viewPassword.setRoundedCornered(height: viewPassword.frame.size.height)
        self.viewConfirmPassword.setRoundedCornered(height: viewConfirmPassword.frame.size.height)
        self.viewPromoCode.setRoundedCornered(height: viewPromoCode.frame.size.height)
        self.btn_Confirm.setRoundedCornered(height: btn_Confirm.frame.size.height)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      //  self.startLoaderAnimation()
       // self.presenter.getAllStates { (data, status, message) in
          //  self.stopLoaderAnimation()
        //    if status{
            //    DispatchQueue.main.async {
              //      self.createStatePicker()
               // }
           // }
        //}
    }
    
    @IBAction func btn_backDidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_SignInAgainDidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_ChechBoxDidClick(_ sender: Any) {
        self.btn_CheckBox.isSelected = !self.btn_CheckBox.isSelected
    }
    
    @IBAction func btn_ConfirmDidClick(_ sender: Any) {
        self.startLoaderAnimation()
        self.presenter.createAccount(firstName: self.textFieldFName.text!, lastName: self.textFieldLName.text!, email: textFieldEmail.text!, countryCode: textFieldCountry.text!, phone: textFieldMobile.text!, cityId: self.presenter.selectedCountryID, location: self.textFieldLocation.text!, password: textPassword.text!, confirmPassword: textConfirmPassword.text!, promoCode: textFieldPromoCode.text!, allowMail: self.presenter.isAllowedMail) { (status, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.showAlertWith(message: message) {
                    if status{
                        //success
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }

        }
    }
    
}
extension CreateAccountView{
    
    func createStatePicker()
    {
        
        statePicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        statePicker?.dataSource = self
        statePicker?.delegate = self
        
        textFieldState.inputView = statePicker
        
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.view.frame.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionDone), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionCancel), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
        view_Accessory.backgroundColor = UIColor(named: "TextColorDark")
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        textFieldState.inputAccessoryView = view_Accessory
        
    }
    
    func createCityPicker()
    {
        
        cityPicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        cityPicker?.dataSource = self
        cityPicker?.delegate = self
        
        textFieldCountry.inputView = cityPicker
        
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.view.frame.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionCityDone), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionCityCancel), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
        view_Accessory.backgroundColor = UIColor(named: "TextColorDark")
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        textFieldCountry.inputAccessoryView = view_Accessory
        
    }
    
    
    
    @objc func selectionDone(_ sender: UIButton ) {
        
        if textFieldState.isFirstResponder
        {
            textFieldState.text = String(describing: self.presenter.arrStates[(statePicker?.selectedRow(inComponent: 0))!].name!)
            self.presenter.selectedStateID = String(describing: self.presenter.arrStates[(statePicker?.selectedRow(inComponent: 0))!].id!)
            self.startLoaderAnimation()
            self.presenter.getAllCities(stateID: self.presenter.selectedStateID) { (data, status, message) in
                self.stopLoaderAnimation()
                if status{
                    DispatchQueue.main.async {
                        self.createCityPicker()
                    }
                }
            }
            
            textFieldState?.resignFirstResponder()
        }
    }
    
    @objc func selectionCancel(_ sender: UIButton ) {
        textFieldState.text = ""
        textFieldState.resignFirstResponder()
    }
    
    @objc func selectionCityDone(_ sender: UIButton ) {
        
        if textFieldCountry.isFirstResponder
        {
            textFieldCountry.text = String(describing: self.presenter.arrCities[(cityPicker?.selectedRow(inComponent: 0))!].name!)
            self.presenter.selectedCountryID = String(describing: self.presenter.arrCities[(cityPicker?.selectedRow(inComponent: 0))!].id!)
            textFieldCountry?.resignFirstResponder()
        }
    }
    
    @objc func selectionCityCancel(_ sender: UIButton ) {
        textFieldCountry.text = ""
        textFieldCountry.resignFirstResponder()
    }
    
}

extension CreateAccountView : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFieldCountryCode{
            let countryPickerStoryBoard = UIStoryboard.init(name: "CountryPicker", bundle: nil)
            let countryPickerVC = countryPickerStoryBoard.instantiateViewController(withIdentifier: "countryPickerViewController") as! CountryPickerViewController
            countryPickerVC.delegate = self
            self.present(countryPickerVC, animated: true, completion: nil)
        }
    }
}

extension CreateAccountView : CountryPickerViewControllerDelegate{
    func didSelectCountry(country: Country) {
        self.textFieldCountryCode.text = country.dialCode!
        //self.imageCountryFlag.image = UIImage(named: country.flag!)
    }
}

extension CreateAccountView: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == statePicker{
            return self.presenter.arrStates.count

        }else{
            return self.presenter.arrCities.count

        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == statePicker{
            return String(describing: self.presenter.arrStates[row].name!)

        }else{
            return String(describing: self.presenter.arrCities[row].name!)

        }

        
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        self.textFieldState.text = String(describing: self.presenter.arrStates[row])
//        self.presenter.objJobProfile.visibleTattoo = String(describing: arrVisibleTatoo[row]) == "Yes" ? 1 : 0
        
    }

}

extension CreateAccountView {
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 40, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 3:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 4:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 5:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 6:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 7:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 8:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 9:
            return 110//getTableViewRowHeightRespectToScreenHeight(givenheight: 99, currentScrenHeight: self.view.bounds.height)
        case 10:
            return 150//getTableViewRowHeightRespectToScreenHeight(givenheight: 136, currentScrenHeight: self.view.bounds.height)
        case 11:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 80, currentScrenHeight: self.view.bounds.height)
        case 12:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 73, currentScrenHeight: self.view.bounds.height)
        case 13:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 69, currentScrenHeight: self.view.bounds.height)
        default:
            return 0.0
        }
    }

}

