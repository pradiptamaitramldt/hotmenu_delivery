//
//  SidemenuVc.swift
//  EazzyEatsDelivery
//
//  Created by Ivica Technologies on 27/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import SDWebImage

class SidemenuVc: UIViewController {

    @IBOutlet var userNameLabel: CustomBoldLabel!
    @IBOutlet var emailIDLabel: CustomBoldLabel!
    @IBOutlet var bgView: UIView!
    @IBOutlet var profileImage: UIImageView!
    
    var utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bgView.setCirculer()
        profileImage.setCirculer()
       
        self.userNameLabel.text = self.utility.Retrive(Constants.Strings.userFullName) as? String
           self.emailIDLabel.text = self.utility.Retrive(Constants.Strings.UserEmail) as? String
        self.profileImage.sd_setImage(with: URL(string: self.utility.Retrive(Constants.Strings.profileImage) as? String ?? "" ), placeholderImage: UIImage(named: "dummyImage"))
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.updateSliderProfileValue(notification:)), name: Notification.Name("UpdateSliderProfileValue"), object: nil)
    }
    @objc func updateSliderProfileValue(notification: Notification) {
        let userID: String = self.utility.Retrive(Constants.Strings.UserID) as! String
        
                       if !userID.isEmpty {
                        DispatchQueue.main.async {
                            self.userNameLabel.text = self.utility.Retrive(Constants.Strings.userFullName) as? String
                                                      self.emailIDLabel.text = self.utility.Retrive(Constants.Strings.UserEmail) as? String
                                                   self.profileImage.sd_setImage(with: URL(string: self.utility.Retrive(Constants.Strings.profileImage) as? String ?? "" ), placeholderImage: UIImage(named: "dummyImage"))
                        }
            }
    }
    @IBAction func profileButtonAction(_ sender: Any) {
        print("profileButtonAction")
        let storyBoard = UIStoryboard(name: "MenuStoryboard", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "EditProfileController") as! EditProfileController
        SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
    }
    
    @IBAction func notificationButtonAction(_ sender: Any) {
        print("notificationButtonAction")
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                   let vc = storyBoard.instantiateViewController(withIdentifier: "NotificationListVc") as! NotificationListVc
                vc.fromVC = "SliderMenu"
                   SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
      
    }
    
    @IBAction func logoutButtonAction(_ sender: Any) {
       
        let alert = UIAlertController(title: "Sign out?", message: "Are sure to sign out", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
                   //Cancel Action
               }))
               alert.addAction(UIAlertAction(title: "Sign out",
                                             style: UIAlertAction.Style.destructive,
                                             handler: {(_: UIAlertAction!) in
                                               //Sign out action
                                                self.logout()
               }))
               self.present(alert, animated: true, completion: nil)
        }
    
    func logout() {
        let fetchAllUserRepository = LogoutRepository()
        fetchAllUserRepository.logout(vc:  self) { (response, success, error) in
          //  self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    
                    let defaults = UserDefaults.standard
                    let dictionary = defaults.dictionaryRepresentation()
                    dictionary.keys.forEach { key in
                        defaults.removeObject(forKey: key)
                    }
            
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "loginWithEmailView") as! LoginWithEmailView
                                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
                  
                }else if response.statuscode == 422 {
                    Utility.showAlert(withMessage: (response.message ?? "" ), onController: self)
                }else {
                    Utility.showAlert(withMessage: (response.message ?? "" ), onController: self)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
}
