//
//  ForgotPasswordPresenter.swift
//  EazzyEats
//
//  Created by Bit Mini on 13/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class ForgotPasswordPresenter: NSObject {

    func forgotpassword(email : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        
        if Utility.isValidEmail(email){
                let param : [String: Any] = ["email" : email]
                WebServiceManager.shared.requestAPI(url: WebServiceConstants.forgotPassword, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: param, httpMethodType: .post) { (data, err) in
                    if let responseData = data{
                        let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: ForgotPasswordResponseModel.self) { (resData, err) in
                            if resData != nil{
                                if resData?.statuscode! == ResponseCode.success.rawValue{
                                    UserDefaultValues.OTP = (resData?.responseData?.forgotPassOtp!)!
                                    completion(true,(resData?.message!)!)
                                }else{
                                    completion(false,(resData?.message!)!)
                                }
                            }else{
                                completion(false, err!.localizedDescription)
                            }
                        }
                    }
                }
        }else{
            completion(false, "Please enter Your Email.")
        }
        
    }

}
