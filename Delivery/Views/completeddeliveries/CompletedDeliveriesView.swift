//
//  CompletedDeliveriesView.swift
//  EazzyEatsDelivery
//
//  Created by Ivica Technologies on 10/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

//MARK:- Objectes
class CompletedDeliveriesView: UIViewController {
    
   
    @IBOutlet var formDateLabel: UILabel!
    @IBOutlet var toDateLabel: UILabel!
    @IBOutlet var statusLabel: CustomBoldLabel!
    @IBOutlet weak var completedDeliveryTableview: UITableView!
   
    var arrayaData = ["211311","233433","311233","577588","344533","2115331","123456","3456784","123432","2369841"]
       var orderDetailsType = "",selectedButton = "",orderID = ""
    var orderDetailsData : OrderDetailsModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        completedDeliveryTableview.register(UINib.init(nibName: "CompletedDeliveriesCell", bundle: nil), forCellReuseIdentifier: "CompletedDeliveriesCell")
        
        completedDeliveryTableview.dataSource = self
        completedDeliveryTableview.delegate = self
        completedDeliveryTableview.backgroundColor = .clear
        /*
        let label = UILabel()
        label.backgroundColor = .clear
        label.numberOfLines = 1
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .left
        label.textColor = .white
        label.text = "Pretoria street,kolkata..."
        self.navigationItem.titleView = label
        
        
        if let navigationBar = self.navigationController?.navigationBar {
            let firstFrame = CGRect(x: 100, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
            let secondFrame = CGRect(x: 300, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
            
            
            //navigationBar.frame.width/5
            let firstLabel = UILabel(frame: firstFrame)
            firstLabel.text = ""
            
            let secondLabel = UILabel(frame: secondFrame)
            secondLabel.text = "Change"
            secondLabel.font = UIFont.boldSystemFont(ofSize: 8)
            
            
            navigationBar.addSubview(firstLabel)
            navigationBar.addSubview(secondLabel)
        }
        */
        
        
        
        
        
        
       // let button1 = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action:#selector(self.clickButton)) // action:#selector(Class.MethodName) for swift 3
      //  self.navigationItem.rightBarButtonItem  = button1
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        print("orderDetailsType",orderDetailsType)
        fetchOrderDetailsData()
    }
        func fetchOrderDetailsData(){
            let fetchAllUserRepository = AllOrderRepository()
             AMShimmer.start(for: completedDeliveryTableview)
            
            fetchAllUserRepository.getOrderDetails(vc:  self, orderID:  orderID) { (response, success, error) in
              //  self.stopLoading()
                DispatchQueue.main.async {
                                       AMShimmer.stop(for: self.completedDeliveryTableview)
                                    }
                if (error != nil) {
                   Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                }
                if (success) {
                    if response.statuscode == 200 {
                   
                        if response.success == false {
                            self.completedDeliveryTableview.isHidden = true
                            Utility.showAlert(withMessage: response.message, onController: self)
                        }
                        else {
                            self.orderDetailsData = response
                            self.completedDeliveryTableview.reloadData()
                        }
                        
                    }else if response.statuscode == 422 {
                        Utility.showAlert(withMessage: (response.message), onController: self)
                    }else {
                        Utility.showAlert(withMessage: (response.message), onController: self)
                    }
                }else{
                    Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                }
            }
        }
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func clickButton(sender: AnyObject){
        
    }
    
    @IBAction func fromDatePickerButtonAction(_ sender: Any) {
        selectedButton = "from"
        let mainStoryBoard = UIStoryboard(name: "DateStoryboard", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController
        redViewController.datePickerDelegate = self
        self.navigationController?.present(redViewController, animated: true, completion: nil)
    }
    @IBAction func toDatePickerButtonAction(_ sender: Any) {
        selectedButton = "to"
        let mainStoryBoard = UIStoryboard(name: "DateStoryboard", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController
        redViewController.datePickerDelegate = self
        self.navigationController?.present(redViewController, animated: true, completion: nil)
    }
    func convertDateFormat(dateTime: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let orderDate = dateFormatter.date(from: dateTime)!
        print(orderDate)
        dateFormatter.dateFormat = "dd MMM yyyy"
        let finalOderDateStr = dateFormatter.string(from: orderDate)
        return finalOderDateStr
    }
    func convertTimeFormat(dateTime: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let orderDate = dateFormatter.date(from: dateTime)!
        print(orderDate)
        dateFormatter.dateFormat = "hh:mm a"
        let finalOderDateStr = dateFormatter.string(from: orderDate)
        return finalOderDateStr
    }
}

//MARK:- TABLEVIEW DELAGATES
extension CompletedDeliveriesView:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orderDetailsData?.success == true {
            return 1
        }
        else {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompletedDeliveriesCell") as! CompletedDeliveriesCell
         if orderDetailsData?.success == true {
            if orderDetailsType == "Assign" {
                cell.labelType.text = "Vendor Contract Number: "
                cell.restaurantNameLabel.text =  "+91 " + String(orderDetailsData?.responseData.vendorID.contactPhone ?? 000000)
            }
            else if orderDetailsType == "Pickup" {
                cell.labelType.text = "Customer Contract Number: "
                cell.restaurantNameLabel.text =  ( orderDetailsData?.responseData.customerID.countryCode  ?? "+91 " ) + String(orderDetailsData?.responseData.customerID.phone ?? 000000)
            }
            else {
                cell.labelType.text = "Customer Contract Number: "
                cell.restaurantNameLabel.text =  ( orderDetailsData?.responseData.customerID.countryCode  ?? "+91 " ) + String(orderDetailsData?.responseData.customerID.phone ?? 000000)
            }
            cell.orderIdLabel.text = orderDetailsData?.responseData.id
            cell.customerNameLabel.text = orderDetailsData?.responseData.customerID.fullName ?? ""
            
          
            cell.dateLabel.text = convertDateFormat(dateTime: (orderDetailsData?.responseData.createdAt)!)
            cell.timeLabel.text = convertTimeFormat(dateTime: (orderDetailsData?.responseData.createdAt)!)
            cell.viewDetailesButton.tag = indexPath.row
            cell.viewDetailesButton.addTarget(self, action: #selector(viewDetails(_:)), for: .touchUpInside)
        }
        
        
        return cell

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 270
    }
    @objc func viewDetails(_ sender:UIButton){
        
        let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "OrderDetailesView") as! OrderDetailesView
        //self.present(redViewController, animated: true, completion: nil)
     //   redViewController.orderDetailData = self.orderDetailsData
        redViewController.orderDetailsType = self.orderDetailsType
        self.navigationController?.pushViewController(redViewController, animated: true)
    }
}

extension CompletedDeliveriesView : DatePickerDelegate {
    func didDateSelected(date: String) {
        print("date",date)
        if selectedButton == "from" {
            formDateLabel.text = date
        }
        else {
            toDateLabel.text = date
        }
    }
}

