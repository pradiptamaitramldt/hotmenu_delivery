//
//  DatePickerController.swift
//  Delivery
//
//  Created by BrainiumSSD on 04/09/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

protocol DatePickerDelegate {
    func didDateSelected(date : String)
}

class DatePickerController: UIViewController {

    @IBOutlet var datePicker: UIDatePicker!
    
    var datePickerDelegate : DatePickerDelegate?
    var selectedDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        datePicker.maximumDate = Date()
    }
    
   @objc func dateChanged(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = components.day, let month = components.month, let year = components.year {
            print("\(day) \(month) \(year)")
        }
    selectedDate = sender.date
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        let dateformat = DateFormatter()
        dateformat.dateFormat = "MM-dd-yyyy"
        let dateStr = dateformat.string(from: selectedDate)
        print("dateStr",dateStr)
        datePickerDelegate?.didDateSelected(date: dateStr)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
