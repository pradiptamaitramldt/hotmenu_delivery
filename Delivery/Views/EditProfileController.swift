//
//  EditProfileController.swift
//  Delivery
//
//  Created by BrainiumSSD on 03/09/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit
import SDWebImage
import AMShimmer

class EditProfileController: BaseViewController {
    
    // MARK:- IBoutlet
    @IBOutlet var userNameTextField: CustomTextField!
    @IBOutlet var drivingLicenceTextField: CustomTextField!
    @IBOutlet var chooseVechileTextField: CustomTextField!
    @IBOutlet var uploadNumberPlateTextField: CustomTextField!
    @IBOutlet var lastNameText: CustomTextField!
    @IBOutlet var profilePicture: UIImageView!
    @IBOutlet var emailIDLabel: CustomBoldLabel!
    @IBOutlet var phoneNumberLabel: CustomBoldLabel!
    @IBOutlet var imageBGView: UIView!
    var utility = Utility()
    var profileData : FetchProfileData?
    let imagePicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        userNameTextField.delegate = self
        drivingLicenceTextField.delegate = self
        chooseVechileTextField.delegate = self
        uploadNumberPlateTextField.delegate = self
        lastNameText.delegate = self
        
        imageBGView.setRoundedCornered(height: self.imageBGView.frame.width / 2)
        profilePicture.setRoundedCornered(height: self.profilePicture.frame.width / 2)
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchProfileData()
    }
    @IBAction func uploadPictureButtonAction(_ sender: Any) {
        alertController()
    }
    
    @IBAction func changeEmailIDButtonAction(_ sender: Any) {
        let mainStoryBoard = UIStoryboard(name: "MenuStoryboard", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "ResetEmailIDController") as! ResetEmailIDController
        self.navigationController?.pushViewController(redViewController, animated: true)
    }
    @IBAction func changePassordButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "MenuStoryboard", bundle: nil)
        let changePasswordVC = storyboard.instantiateViewController(identifier: "ChangePasswordController") as! ChangePasswordController
        self.navigationController?.pushViewController(changePasswordVC, animated: true)
    }
    func alertController() {
        
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        // 2
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Camera")
            self.imageCamera()
        })
        let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Gallery")
            self.imageGallery()
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    
    func fetchProfileData() {
        let fetchAllUserRepository = FetchProfileRepository()
        AMShimmer.start(for: userNameTextField)
        AMShimmer.start(for: lastNameText)
        AMShimmer.start(for: drivingLicenceTextField)
        AMShimmer.start(for: chooseVechileTextField)
        AMShimmer.start(for: uploadNumberPlateTextField)
        AMShimmer.start(for: emailIDLabel)
        AMShimmer.start(for: phoneNumberLabel)
        AMShimmer.start(for: profilePicture)
        fetchAllUserRepository.fetchProfile(vc:  self) { (response, success, error) in
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.userNameTextField)
                AMShimmer.stop(for: self.lastNameText)
                AMShimmer.stop(for: self.drivingLicenceTextField)
                AMShimmer.stop(for: self.chooseVechileTextField)
                AMShimmer.stop(for: self.uploadNumberPlateTextField)
                AMShimmer.stop(for: self.emailIDLabel)
                AMShimmer.stop(for: self.phoneNumberLabel)
                AMShimmer.stop(for: self.profilePicture)
            }
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    
                    print("data",response.responseData)
                    self.profileData = response
                    self.emailIDLabel.text = response.responseData.email
                    self.phoneNumberLabel.text = response.responseData.countryCode + " " +  String(response.responseData.phone)
                    self.userNameTextField.text = response.responseData.firstName 
                    self.lastNameText.text = response.responseData.lastName
                    self.chooseVechileTextField.text = response.responseData.vehicle
                    self.uploadNumberPlateTextField.text = response.responseData.numberPlate
                    self.drivingLicenceTextField.text = response.responseData.driverLicense
                    self.profilePicture.sd_setImage(with: URL(string: self.profileData?.responseData.profileImage ?? ""), placeholderImage: UIImage(named: "dummyImage"))
                    let fName = response.responseData.firstName
                    let lName = response.responseData.lastName
                    let fullName = fName + " " + lName
                    self.utility.Save(fullName, keyname: Constants.Strings.userFullName)
                    NotificationCenter.default.post(name: Notification.Name("UpdateSliderProfileValue"), object: nil)
                }else if response.statuscode == 422 {
                    Utility.showAlert(withMessage: response.message, onController: self)
                }else {
                    Utility.showAlert(withMessage: response.message, onController: self)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    // MARK:- IBAction
    @IBAction func backButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeDeliveryView") as! HomeDeliveryView
        //            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        SlideNavigationController.sharedInstance()?.popAllAndSwitch(to:vc , withSlideOutAnimation: true, andCompletion: nil)
    }
    
    @IBAction func updateButtonAction(_ sender: Any) {
        
        if !Utility.isValidName(userNameTextField.text ?? "") {
            showAlert(message: "Please provide valid first name")
        }
        else if !Utility.isValidName(lastNameText.text ?? "") {
            showAlert(message: "Please provide valid last name")
        }
        else if !Utility.isValidText(drivingLicenceTextField.text ?? "") {
            showAlert(message: "Please provide valid driving licence")
        }
        else if !Utility.isValidText(chooseVechileTextField.text ?? "") {
            showAlert(message: "Please provide vechile name")
        }
        else if !Utility.isValidText(uploadNumberPlateTextField.text ?? "") {
            showAlert(message: "Please upload valid number plate")
        }
        else {
            let userID = self.utility.Retrive(Constants.Strings.UserID) as! String
            let params =  ["deliveryBoyId" : userID,
                           "firstName" : userNameTextField.text!,
                           "lastName" : lastNameText.text!,
                           "vehicle" : chooseVechileTextField.text!,
                           "numberPlate" : uploadNumberPlateTextField.text! ,
                           "driverLicense" : drivingLicenceTextField.text!,
                           "userType" : "DELIVERY_BOY",
                           "loginType" :"GENERAL",
                           "countryCode" : profileData?.responseData.countryCode ?? ""]
            editProfileData(parameter: params)
        }
        
    }
    func editProfileData(parameter : [String : String] ) {
        let fetchAllUserRepository = EditProfileRepository()
        AMShimmer.start(for: userNameTextField)
        AMShimmer.start(for: lastNameText)
        AMShimmer.start(for: drivingLicenceTextField)
        AMShimmer.start(for: chooseVechileTextField)
        AMShimmer.start(for: uploadNumberPlateTextField)
        AMShimmer.start(for: profilePicture)
        fetchAllUserRepository.editProfile(vc: self, params: parameter) { (response, success, error) in
            //  self.stopLoading()
            AMShimmer.stop(for: self.userNameTextField)
            AMShimmer.stop(for: self.lastNameText)
            AMShimmer.stop(for: self.drivingLicenceTextField)
            AMShimmer.stop(for: self.chooseVechileTextField)
            AMShimmer.stop(for: self.uploadNumberPlateTextField)
            AMShimmer.stop(for: self.profilePicture)
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    
                    print("data",response.responseData)
                    let fName = (self.userNameTextField.text!)
                    let lName = (self.lastNameText.text!)
                    let fullName = fName + " " + lName
                    self.utility.Save(fullName, keyname: Constants.Strings.userFullName)
                    NotificationCenter.default.post(name: Notification.Name("UpdateSliderProfileValue"), object: nil)
                }else if response.statuscode == 422 {
                    Utility.showAlert(withMessage: response.message, onController: self)
                }else {
                    Utility.showAlert(withMessage: response.message, onController: self)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
}

extension EditProfileController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
extension EditProfileController : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imageGallery() {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imageCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        let profileImage = self.fixImageOrientation(img!)
        self.profilePicture.image = profileImage
        
        let profileUploadRepository = ProfileImageUploadRepository()
        profileUploadRepository.uploadProfileImage(profileImage: profileImage, vc: self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                let statusCode = response["STATUSCODE"] as! String
                let message = response["message"]
                if statusCode == "200" {
                    
                  //  Utility.showAlert(withMessage: message as! String, onController: self)
                    let profileImage  =    ( (response.value(forKey: "response_data") as! NSDictionary).object(forKey: "profileImage") as! String)
                    //  print("profileImage : \(profileImage)")
                    self.utility.Save(profileImage, keyname: Constants.Strings.profileImage)
                    NotificationCenter.default.post(name: Notification.Name("UpdateSliderProfileValue"), object: nil)
                }
                else {
                    Utility.showAlert(withMessage: message as! String, onController: self)
                }
                
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func fixImageOrientation(_ image: UIImage)->UIImage {
        UIGraphicsBeginImageContext(image.size)
        image.draw(at: .zero)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? image
    }
}
