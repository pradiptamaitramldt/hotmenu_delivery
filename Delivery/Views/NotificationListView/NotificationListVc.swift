//
//  NotificationListVc.swift
//  EazzyEatsDelivery
//
//  Created by Ivica Technologies on 05/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class NotificationListVc: UIViewController {
    
    // MARK:- IBOutlet
    
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var notificationLiestTableview: UITableView!
    
    // MARK:- Other Variables
    
    var arrayaData = ["Notifications provide short, timely information about events in your app while it's not in use. This page teaches you how","Notifications provide short, timely information about events in your app while it's not in use. This page teaches you how","Notifications provide short, timely information about events in your app w"]
    var fromVC = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationLiestTableview.register(UINib.init(nibName: "NotificationListCell", bundle: nil), forCellReuseIdentifier: "NotificationListCell")
        notificationLiestTableview.dataSource = self
        notificationLiestTableview.delegate = self
        notificationLiestTableview.backgroundColor = .white
        
        let label = UILabel()
        label.backgroundColor = .clear
        label.numberOfLines = 1
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .left
        label.textColor = .white
        label.text = "Notifications"
        self.navigationItem.titleView = label
        
        
    //    let button1 = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action:#selector(self.clickButton)) // action:#selector(Class.MethodName) for swift 3
     //   self.navigationItem.rightBarButtonItem  = button1

        // Do any additional setup after loading the view.
        
//        if fromVC == "SliderMenu" {
//            headerView.isHidden = false
//        }
//        else {
//            headerView.isHidden = true
//        }
    }
    
    
    @objc func clickButton(sender: AnyObject){
        
       
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                          let vc = storyboard.instantiateViewController(withIdentifier: "HomeDeliveryView") as! HomeDeliveryView
            
               SlideNavigationController.sharedInstance()?.popAllAndSwitch(to:vc , withSlideOutAnimation: true, andCompletion: nil)
    }
    

}

extension NotificationListVc:UITableViewDataSource,UITableViewDelegate {
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayaData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationListCell") as! NotificationListCell
        
        
        cell.DiscLabel.text = arrayaData[indexPath.row]
        
        return cell

    }
}



