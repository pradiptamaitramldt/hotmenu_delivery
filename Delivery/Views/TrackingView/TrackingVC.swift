//
//  TrackingVC.swift
//  EazzyEatsDelivery
//
//  Created by Ivica Technologies on 27/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import GoogleMaps

class TrackingVC: UIViewController {

    @IBOutlet var timeLabel: CustomNormalLabel!
    @IBOutlet var showArrivingTimeView: UIView!
    @IBOutlet var mapImage: UIImageView!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var contractNumberLabel: CustomBoldLabel!
    
    var orderDetailsData : OrderDetailsModel?
    var locationManager: CLLocationManager!
    var currentLocation: CLLocation?,startLocation : CLLocationCoordinate2D?,destinationLocation : CLLocationCoordinate2D?
         var zoomLevel: Float = 18.0
      var locationType = "",orderDetailsType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*
        let label = UILabel()
        label.backgroundColor = .clear
        label.numberOfLines = 1
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .left
        label.textColor = .white
        label.text = "Pretoria street,kolkata..."
        self.navigationItem.titleView = label
        
        
        
        
        if let navigationBar = self.navigationController?.navigationBar {
            let firstFrame = CGRect(x: 100, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
            let secondFrame = CGRect(x: 300, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
            
            
            //navigationBar.frame.width/5
            let firstLabel = UILabel(frame: firstFrame)
            firstLabel.text = ""
            
            let secondLabel = UILabel(frame: secondFrame)
            secondLabel.text = "Change"
            secondLabel.font = UIFont.boldSystemFont(ofSize: 8)
            
            
            navigationBar.addSubview(firstLabel)
            navigationBar.addSubview(secondLabel)
        }
        */
      //  let button1 = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action:#selector(self.clickButton)) // action:#selector(Class.MethodName) for swift 3
       // self.navigationItem.rightBarButtonItem  = button1
        
        if orderDetailsType == "Completed" {
                   mapView.isHidden = true
                   showArrivingTimeView.isHidden = true
               }
        else {
            if orderDetailsType == "Pickup" {
                timeLabel.text = "You are " +  "10" + "min away from delivery the food to the customer"
               }
                else {
              timeLabel.text =  "You are " +  "15" + "min away from delivery the food to the vendoor"
                   }
            locationManager = CLLocationManager()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.distanceFilter = 50
            locationManager.startUpdatingLocation()
            locationManager.delegate = self
            mapView.delegate = self
            mapView.isHidden = false
            showArrivingTimeView.isHidden = false
        }
        contractNumberLabel.text = "For any help. Call the vendoor at " + "+91 " + String(orderDetailsData?.responseData.vendorID.contactPhone ?? 000000)
    }
    
    
    
    @objc func clickButton(sender: AnyObject){
        
        let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "NotificationListVc") as! NotificationListVc
        redViewController.fromVC = "TrackingVC"
        self.navigationController?.pushViewController(redViewController, animated: true)
        
    }

    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension TrackingVC : CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        currentLocation = location
        print("CurrentLocation : \(location)")
        movedToCurrentLocation(latitude : location.coordinate.latitude, longitude  : location.coordinate.longitude, type: "Default")
        //ShowDriver
        // movedToCurrentLocation(latitude : location.coordinate.latitude, longitude  : location.coordinate.longitude, type: "ShowDriver")
        locationManager.stopUpdatingLocation()
        // listLikelyPlaces()
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        @unknown default:
            fatalError()
        }
    }
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    func movedToCurrentLocation(latitude : Double, longitude : Double,type : String ) {
        
        var newZoomLevel = 0.0
        mapView.clear()
        if type == "Default" {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude )
            let iconView = UIView()
            iconView.backgroundColor = UIColor(red: 0, green: 158/255, blue: 54/255, alpha: 1.0)
            iconView.frame.size = CGSize(width: 35, height: 35)
           // iconView.setCirculer()
           // iconView.setBorder(color: UIColor.white.cgColor, borderWidth: 1.0)
//            let markerImageView = UIImageView()
//            markerImageView.image = UIImage(named: "pickup_icon")!.withRenderingMode(.alwaysTemplate)
//            markerImageView.tintColor = UIColor.white
//            markerImageView.frame.size = CGSize(width: 12.0, height: 18.0)
//            markerImageView.center = iconView.center
//            iconView.addSubview(markerImageView)
//            marker.iconView = iconView
//            marker.map = mapView
            newZoomLevel = Double(zoomLevel)
        }
        else if type == "Pickup" {
            
            newZoomLevel = 14.0
            let state_marker = GMSMarker()
            state_marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude )
//            let iconView = UIView()
//            iconView.backgroundColor = .black
//            iconView.frame.size = CGSize(width: 35, height: 35)
//            iconView.setCirculer()
//            iconView.setBorder(color: UIColor.white.cgColor, borderWidth: 1.0)
//            let markerImageView = UIImageView()
//            markerImageView.image = UIImage(named: "pickup_icon")!.withRenderingMode(.alwaysTemplate)
//            markerImageView.tintColor = UIColor.white
//            markerImageView.frame.size = CGSize(width: 12.0, height: 18.0)
//            markerImageView.center = iconView.center
//            iconView.addSubview(markerImageView)
       //     state_marker.iconView = iconView
            state_marker.map = mapView
            
        }
        else if type == "ShowDriver" {
            newZoomLevel = 14.0
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: currentLocation?.coordinate.latitude ?? 0.0, longitude: currentLocation?.coordinate.longitude ?? 0.0)
            marker.title = ""
            marker.snippet = ""
            marker.map = mapView
            
            let state_marker = GMSMarker()
            state_marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude )
            let iconView = UIView()
            //  iconView.backgroundColor = .black
            iconView.frame.size = CGSize(width: 60, height: 55)
            //                       iconView.setCirculer()
            //                       iconView.setBorder(color: UIColor.white.cgColor, borderWidth: 1.0)
            let markerImageView = UIImageView()
            markerImageView.image = UIImage(named: "truck_icon")!
            markerImageView.frame.size = CGSize(width: 60.0, height: 55.0)
            markerImageView.center = iconView.center
            iconView.addSubview(markerImageView)
            // iconView.rotate(duration: 10.0)
            state_marker.iconView = iconView
            state_marker.map = mapView
        }
        let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                              longitude: longitude,
                                              zoom: Float(newZoomLevel))
        
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
    }
}
extension TrackingVC : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        print("Marker Lattitude : ",marker.position.latitude)
        return true
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        print("lat : ",coordinate.latitude)
        print("long : ", coordinate.longitude)
    }
    func drawRoute() {
        
        mapView.clear()
        /*
        if locationArr.count >= 2 {
            for i in 0 ... locationArr.count - 2{
                // print("address",locationArr[i].locationName ?? "")
                //print("lat ",locationArr[i].locationLatitude ?? 0.0)
                //print("long ",locationArr[i].locationLongitude ?? 0.0)
                startLocation = CLLocationCoordinate2D(latitude: locationArr[i].locationLatitude ?? 0.0, longitude: locationArr[i].locationLongitude ?? 0.0)
                destinationLocation = CLLocationCoordinate2D(latitude: locationArr[i+1].locationLatitude ?? 0.0, longitude: locationArr[i+1].locationLongitude ?? 0.0)
                getPolylineRoute(getStartLocation: startLocation!, getDestinationLocation: destinationLocation!)
            }
        }
        for i in 0 ... locationArr.count - 1 {
            
            let state_marker = GMSMarker()
            state_marker.position = CLLocationCoordinate2D(latitude: locationArr[i].locationLatitude ?? 0.0, longitude: locationArr[i].locationLongitude ?? 0.0 )
            
            let markerView = UIImageView()
            if i == locationArr.count - 1 {
                
                let iconView = UIView()
                iconView.backgroundColor = .black
                iconView.frame.size = CGSize(width: 35, height: 35)
                iconView.setCirculer()
                iconView.setBorder(color: UIColor.white.cgColor, borderWidth: 1.0)
                let markerImageView = UIImageView()
                markerImageView.image = UIImage(named: "pickup_icon")!.withRenderingMode(.alwaysTemplate)
                markerImageView.tintColor = UIColor.white
                markerImageView.frame.size = CGSize(width: 12.0, height: 18.0)
                markerImageView.center = iconView.center
                iconView.addSubview(markerImageView)
                state_marker.iconView = iconView
            }
            else  {
                markerView.image = UIImage(named: "destination_icon")!
                markerView.frame.size = CGSize(width: 60.0, height: 55.0)
                state_marker.iconView = markerView
            }
            state_marker.map = mapView
        }
        //destination_icon
        let camera = GMSCameraPosition.camera(withLatitude: locationArr[0].locationLatitude ?? 0.0,
                                              longitude: locationArr[0].locationLongitude ?? 0.0,
                                              zoom: Float(14.0))
        
        mapView.animate(to: camera)
 */
    }
    
    func getPolylineRoute(getStartLocation : CLLocationCoordinate2D,getDestinationLocation : CLLocationCoordinate2D){
        
        let startLocation = getStartLocation
        let destinationLocation = getDestinationLocation
        let origin = "\(startLocation.latitude),\(startLocation.longitude)"
        let destination = "\(destinationLocation.latitude),\(destinationLocation.longitude)"
        
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&key=AIzaSyCFbzGP1MzoWZe58y5B9_K-KP8qLmejGUM"
        //  let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&sensor=false"
        print("url : ",urlString)
        guard let url = URL(string: urlString)
            else
        {
            print("fjghfjhdgj")
            return
        }
        let urlRequest = URLRequest(url: url)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }else{
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        DispatchQueue.global(qos: .background).async {
                            let arr = json["routes"] as! NSArray
                            if arr.count == 0
                            {
                                print("arr value does not get")
                                //return
                            }
                            else
                            {
                                let dic = arr[0] as! NSDictionary
                                // print("dict value get")
                                let dic1 = dic["overview_polyline"] as! NSDictionary
                                let points = dic1["points"] as! String
                                let arrLegs = (arr[0] as! NSDictionary).object(forKey: "legs") as! NSArray
                                //   print(arrLegs)
                                let arrSteps = arrLegs[0] as! NSDictionary
                                let dicDictance = arrSteps["distance"] as! NSDictionary
                                let dicDuration = arrSteps["duration"] as! NSDictionary
                                let distance = dicDictance["text"] as! String
                                let duration = dicDuration["text"] as! String
                                //      print("distance : = ",distance )
                                //     print("duration : = ",duration)
                                
                                //                            self.timeArr.add(duration)
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                                    
                                    let path = GMSPath(fromEncodedPath: points)
                                    let polygon = GMSPolyline(path: path)
                                   // polygon.strokeColor = UIColor(red: 0, green: 158/255, blue: 54/255, alpha: 1.0)
                                    polygon.strokeColor = .black
                                    polygon.strokeWidth = 5
                                    polygon.map = self.mapView
//                                    if ( self.currentStatus ) == "STARTED" {
//                                        self.driverProfileView.arrivingTimeLabel.text = "Transit in  " + duration
//                                    }
//                                    else {
//                                        self.driverProfileView.arrivingTimeLabel.text = "Arriving in " + duration
//                                    }
                                    
                                })
                            }
                        }
                    }
                }
                catch{
                    //print("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
}
