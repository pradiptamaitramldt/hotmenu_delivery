//
//  SideMenuRootView.swift
//  Nexopt
//
//  Created by Pradipta on 05/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class SideMenuRootView: LFSideViewController, LFSideViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.contentViewController = storyboard.instantiateViewController(withIdentifier: "NavigationViewController")
        
        self.leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenu")
        
        if let sideViewController = self.sideViewController() {
            sideViewController.delegate = self
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
