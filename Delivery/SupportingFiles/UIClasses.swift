  //
//  UIClasses.swift
//  EazzyEats
//
//  Created by Bit Mini on 06/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation
import UIKit
  
  @IBDesignable
  class RoundedButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = frame.size.height / 2
        clipsToBounds = true
        imageView?.contentMode = .scaleAspectFit
    }
    
    @IBInspectable var borderwidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
  
    
    @IBInspectable var fontSize: CGFloat = 20.0 {
        didSet {
            titleLabel?.font = UIFont(name: "Quicksand-Medium", size: fontSize)
        }
    } 
    
    @IBInspectable var bordercolor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var bgColor: UIColor? {
        didSet {
            backgroundColor = bgColor
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted{
                backgroundColor = backgroundColor?.withAlphaComponent(0.6)
            }else{
                backgroundColor = backgroundColor?.withAlphaComponent(1.0)
            }
        }
    }
  }
  
  
  @IBDesignable
  class CustomNormalLabel: UILabel {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        font = UIFont(name: "Quicksand-Medium", size: fontSize)

    }
    
    @IBInspectable var fontSize: CGFloat = 17.0 {
        didSet {
            font = UIFont(name: "Quicksand-Medium", size: fontSize)
        }
    }
    
  }
  
  @IBDesignable
  class CustomBoldLabel: UILabel {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        font = UIFont(name: "Quicksand-Medium", size: fontSize)
    }
    
    @IBInspectable var fontSize: CGFloat = 17.0 {
        didSet {
            font = UIFont(name: "Quicksand-Medium", size: fontSize)
        }
    }
    
  }
  
  @IBDesignable
  class NormalButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView?.contentMode = .scaleAspectFit
        titleLabel?.font = UIFont(name: "Quicksand-Medium", size: fontSize)

    }
    
  
    
    @IBInspectable var fontSize: CGFloat = 17.0 {
        didSet {
            titleLabel?.font = UIFont(name: "Quicksand-Medium", size: fontSize)
        }
    }
    
    
    @IBInspectable var bgColor: UIColor? {
        didSet {
            backgroundColor = bgColor
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted{
                backgroundColor = backgroundColor?.withAlphaComponent(0.6)
            }else{
                backgroundColor = backgroundColor?.withAlphaComponent(1.0)
            }
        }
    }
  }

  
  @IBDesignable
  class NormalBoldButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView?.contentMode = .scaleAspectFit
        titleLabel?.font = UIFont(name: "Quicksand-Medium", size: fontSize)

    }
    
    @IBInspectable var fontSize: CGFloat = 17.0 {
        didSet {
            titleLabel?.font = UIFont(name: "Quicksand-Medium", size: fontSize)
        }
    }
    
    
    @IBInspectable var bgColor: UIColor? {
        didSet {
            backgroundColor = bgColor
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted{
                backgroundColor = backgroundColor?.withAlphaComponent(0.6)
            }else{
                backgroundColor = backgroundColor?.withAlphaComponent(1.0)
            }
        }
    }
  }
  
  
  @IBDesignable
  class CustomTextField: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        font = UIFont(name: "Quicksand-Medium", size: 16.0)
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
    }
    
  }
