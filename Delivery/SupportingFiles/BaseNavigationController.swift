//
//  BaseNavigationController.swift
//  Delivery
//
//  Created by Pallab on 28/08/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.isTranslucent = false
    }

}
