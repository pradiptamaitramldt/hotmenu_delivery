//
//  GlobalMethods.swift
//  PurchasePal
//
//  Created by Bit Mini on 26/11/19.
//  Copyright © 2019 Bit Mini. All rights reserved.
//

import Foundation
import CoreGraphics
import LocalAuthentication

class UserDefaultValues {
    static var userID : String?{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "userID")
        }
        get{
            return UserDefaults.standard.value(forKey: "userID") as? String
        }
    }
    
    static var authToken : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "token")
        }
        get{
            return UserDefaults.standard.value(forKey: "token") as! String
        }
    }
    
    static var OTP : String{
           set{
               UserDefaults.standard.setValue(newValue, forKey: "token")
           }
           get{
               return UserDefaults.standard.value(forKey: "token") as! String
           }
       }
    
    static var userName : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "userName")
        }
        get{
            return UserDefaults.standard.value(forKey: "userName") as! String
        }
    }
    
    static var FirstName : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "FName")
        }
        get{
            return UserDefaults.standard.value(forKey: "FName") as! String
        }
    }
    
    static var LastName : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "LName")
        }
        get{
            return UserDefaults.standard.value(forKey: "LName") as! String
        }
    }

    
    static var email : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "email")
        }
        get{
            return UserDefaults.standard.value(forKey: "email") as! String
        }
    }
    
    static var phone : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "phone")
        }
        get{
            return UserDefaults.standard.value(forKey: "phone") as! String
        }
    }
    
   
    static var loginID : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "loginID")
        }
        get{
            return UserDefaults.standard.value(forKey: "loginID") as! String
        }
    }
}

var designedScreenheight : CGFloat = 896

func getTableViewRowHeightRespectToScreenHeight(givenheight : CGFloat, currentScrenHeight : CGFloat) -> CGFloat {
    return (givenheight / designedScreenheight) * currentScrenHeight
    
}


struct Utility {
     let prefs = UserDefaults.standard
    static func isNetworkReachable() -> Bool {
        return (Reachability()?.connection != Reachability.Connection.none)
    }
    
    func Save(_ str:String,keyname:String) {
        prefs.setValue(str, forKey: keyname)
        prefs.synchronize()
    }
    
    func Retrive(_ str:String)-> AnyObject {
        let retrivevalue =  prefs.value(forKey: str)
        if retrivevalue != nil {
            return retrivevalue! as AnyObject
        }
        return "" as AnyObject
    }
    
    public static func log(_ items: Any?) {
        print(items ?? "Nil value")
    }
    
    //Validate Email
    public static func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    //validate password
    public static func isValidMobileNumber(_ mobile: String) -> Bool {
        let phoneNumberRegex = "^[1-9]\\d{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: mobile)
        return isValidPhone
    }
    
    //validate Password
    public static func isValidPassword(_ password : String) -> Bool {
        do {//^(.{0,7}|[^0-9]*|[^A-Z]*|[a-zA-Z0-9]*)$
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$", options: .caseInsensitive)
            if(regex.firstMatch(in: password, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, password.count)) != nil){

                if(password.count>=8 && password.count<=20){
                    return true
                }else{
                    return false
                }
            }else{
                return false
            }
        } catch {
            return false
        }
    }
    
    //validate Password
     public static func isValidText(_ text : String) -> Bool {
        if !text.trimmingCharacters(in: .whitespaces).isEmpty {
            return true
        }
        else {
            return false
        }
     }
 public static   func isValidName(_ text : String) -> Bool {
        if text.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                //string blank
                return false
            }
            if text.rangeOfCharacter(from: .whitespaces) != nil {
                //string contains white spaces
                return false
            }
    //        let charset = CharacterSet(charactersIn: "1234567890~`!@#$%^&*()_=+[{}]|:;<,>?/")
    //        if self.rangeOfCharacter(from: charset) != nil {
    //            return false
    //        }
            let nameRegex = "^[a-zA-Z'\\-]{2,20}$"
            return NSPredicate(format: "SELF MATCHES %@", nameRegex).evaluate(with: text)
            
         //   return true
            /*
             let nameRegex = "^[a-zA-Z'\\-]{2,20}$"
             return NSPredicate(format: "SELF MATCHES %@", nameRegex).evaluate(with: self)
             */
        }
    static func showAlert(withMessage : String, onController : UIViewController) {
          let alertController = UIAlertController(title: Constants.Strings.AppName, message: withMessage, preferredStyle: .alert)
          let okAction = UIAlertAction(title: Constants.Strings.okAction, style: .default, handler: nil)
          alertController.addAction(okAction)
          onController.present(alertController, animated: true, completion: nil)
      }
      
      static func showAlert(withTitle: String, andMessage: String, onController: UIViewController, actions: [UIAlertAction]) {
          let alertController = UIAlertController(title: withTitle, message: andMessage, preferredStyle: .alert)
          for action in actions {
              alertController.addAction(action)
          }
          onController.present(alertController, animated: true, completion: nil)
      }
    
    
    
    
}

