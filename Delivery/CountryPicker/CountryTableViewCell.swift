//
//  CountryTableViewCell.swift
//  Global
//
//  Created by Mukesh Singh on 14/01/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var imageViewCountryFlag: UIImageView!
    @IBOutlet weak var labelCountryNameAndISDCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .white
        self.contentView.backgroundColor = .white
        self.labelCountryNameAndISDCode.textColor = .black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
