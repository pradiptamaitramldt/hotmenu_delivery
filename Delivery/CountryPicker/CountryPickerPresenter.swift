//
//  CountryPickerPresenter.swift
//  Global
//
//  Created by Mukesh Singh on 14/01/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//
/*
import Foundation

class CountryPickerPresenter: CountryPickerPresenterProtocol {
    
    private weak var view: CountryPickerViewProtocol?
    weak var delegate: CountryPickerDelegate?
    internal var countries: [Country] = []
    internal var countriesTemp: [Country] = []
    
    required init(_ view: CountryPickerViewProtocol) {
        self.view = view
    }
    
    required init(_ view: CountryPickerViewProtocol, delegate: CountryPickerDelegate) {
        self.view = view
        self.delegate = delegate
    }
    
    deinit {
        Utility.log("CountryPickerPresenter deinit")
    }
    
    func loadCountriesJson() {
        guard let view = self.view else { return }
        if let url = Bundle.main.url(forResource: "countryCodes", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                self.countries = try decoder.decode([Country].self, from: data)
                self.countriesTemp = self.countries
                view.dataFinishedLoading()
            } catch {
                Utility.log("error:\(error)")
                view.showErrorAlertWithTitle(title: "", message: "Error fetching country list")
            }
        }
        view.showErrorAlertWithTitle(title: "", message: "Error fetching country list")
    }
    
    func didSelectCountry(country: Country) {
        guard let delegate = self.delegate else { return }
        delegate.didSelectCountry(country: country)
    }
    
    func filterCountryList(filterString: String) {
        guard let view = self.view else { return }
        self.countriesTemp = self.countries
        if filterString.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            self.countriesTemp = self.countriesTemp.filter {
                $0.name?.range(of: filterString, options: .caseInsensitive) != nil
            }
        }
        view.dataFinishedLoading()
        return
    }
}
*/
