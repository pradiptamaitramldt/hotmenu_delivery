//
//  VerifyOTPCommand.swift
//  Delivery
//
//  Created by BrainiumSSD on 11/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation
class VerifyOTPCommand: BaseCommand {
    var email : String?
    var OTP : String?
    
    init(email: String?, OTP: String?) {
        self.email = email
        self.OTP = OTP
    }
    
    override func validate(completion: @escaping(Bool, String?) -> Void) {
        
        if(self.email == nil || self.email?.count == 0){
            completion(false, "Please enter email")
        }else if(Utility.isValidEmail(self.email ?? "")==false){
            completion(false, "Please enter valid email")
        }else if(self.OTP == nil || self.OTP?.count == 0){
            completion(false, "Please enter OTP")
        }else{
            completion(true, "")
        }
    }
 
}
