//
//  LoginCommand.swift
//  Nexopt
//
//  Created by Pradipta on 25/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class LoginCommand: BaseCommand {
    var email : String?
    var password : String?
    
    init(email: String?, password: String?) {
        self.email = email
        self.password = password
    }
    
    override func validate(completion: @escaping(Bool, String?) -> Void) {
        
        if(self.email == nil || self.email?.count == 0){
            completion(false, "Please enter email")
        }else if(self.isValidEmail(emailStr: self.email)==false){
            completion(false, "Please enter valid email")
        }else if(self.password == nil || self.password?.count == 0){
            completion(false, "Please enter password")
        }else{
            completion(true, "")
        }
    }
    
    func isValidEmail(emailStr:String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
}
