//
//  EditProfileRequest.swift
//  Delivery
//
//  Created by BrainiumSSD on 10/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit
import Alamofire

class EditProfileRequest: NSObject {

    var utility = Utility()
    private func printJSON(apiName: String, data: Data) {
        print("API name: ", apiName)
        print(String(data: data, encoding: String.Encoding.utf8) ?? "")
    }
    func presentAlertWithTitle(title: String, message : String, vc: UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in print("Youve pressed OK Button")
        }
        alertController.addAction(OKAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    func editProfileData<T : APIResponseParentModel>(vc: UIViewController,parameter : [String : String], hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            
            let fetchAllUserUrl = Constants.APIPath.editProfile
            let token = self.utility.Retrive(Constants.Strings.Token) as! String
            let headers: HTTPHeaders = [
                            "Content-Type": "application/x-www-form-urlencoded",
                            "Authorization": "Bearer \(token)"
                        ]
           
            Alamofire.request(fetchAllUserUrl, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: headers).responseData { response in
                
                switch(response.result){
                case .success(_):
                    self.printJSON(apiName: fetchAllUserUrl, data: response.result.value!)
                    print(response.result.description)
                    if response.result.value != nil {
                        completion(response.result.value, "", true)
                    }
                    //self.presentAlertWithTitle(title: "Alert", message: (response.result.message)!, vc: vc)
                    break
                case .failure(_):
                    print(response.result.error ?? "Fail")
                    self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                    break
                }
            }
        }
    }
    
}
