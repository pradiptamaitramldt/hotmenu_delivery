//
//  AllDeliveriesRequest.swift
//  Delivery
//
//  Created by BrainiumSSD on 12/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit
import Alamofire

class AllOrderRequest: NSObject {
     var utility = Utility()
        
        private func printJSON(apiName: String, data: Data) {
            print("API name: ", apiName)
            print(String(data: data, encoding: String.Encoding.utf8) ?? "")
        }
        func presentAlertWithTitle(title: String, message : String, vc: UIViewController)
        {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) {
                (action: UIAlertAction) in print("Youve pressed OK Button")
            }
            alertController.addAction(OKAction)
            vc.present(alertController, animated: true, completion: nil)
        }
        
    func getAllOrderList<T : APIResponseParentModel>(vc: UIViewController,lat: Double,long : Double, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
            
            if Utility.isNetworkReachable() {
    //            if hud{
    //                load.show(views: vc.view)
    //            }
                let changeEmailUrl = Constants.APIPath.getAllOrderList + "?latitude=" + String(lat) + "&longitude=" + String(long)
                let token = self.utility.Retrive(Constants.Strings.Token) as! String
                let headers: HTTPHeaders = [
                                "Content-Type": "application/x-www-form-urlencoded",
                                "Authorization": "Bearer \(token)"
                            ]
                
                Alamofire.request(changeEmailUrl, method: .get, parameters: [:], encoding: URLEncoding.default, headers: headers).responseData { response in
    //                if hud{
    //                    self.load.hide(delegate: vc)
    //                }
                    switch(response.result){
                    case .success(_):
                        self.printJSON(apiName: changeEmailUrl, data: response.result.value!)
                        print(response.result.description)
                        if response.result.value != nil {
                            completion(response.result.value, "", true)
                        }
                        break
                    case .failure(_):
                        print(response.result.error ?? "Fail")
                        self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                        break
                    }
                }
            }
        }
    
    func getOrderDetails<T : APIResponseParentModel>(vc: UIViewController,orderID : String, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
            
            if Utility.isNetworkReachable() {
    //            if hud{
    //                load.show(views: vc.view)
    //            }
                let changeEmailUrl = Constants.APIPath.getOrderDetails + orderID
                let token = self.utility.Retrive(Constants.Strings.Token) as! String
                let headers: HTTPHeaders = [
                                "Content-Type": "application/x-www-form-urlencoded",
                                "Authorization": "Bearer \(token)"
                            ]
                let params = ["orderListId" : orderID]
                Alamofire.request(changeEmailUrl, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseData { response in
    //                if hud{
    //                    self.load.hide(delegate: vc)
    //                }
                    switch(response.result){
                    case .success(_):
                        self.printJSON(apiName: changeEmailUrl, data: response.result.value!)
                        print(response.result.description)
                        if response.result.value != nil {
                            completion(response.result.value, "", true)
                        }
                        break
                    case .failure(_):
                        print(response.result.error ?? "Fail")
                        self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                        break
                    }
                }
            }
        }
    
    func changeOrderStatus<T : APIResponseParentModel>(vc: UIViewController,parameter : [String : Any], hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
            
            if Utility.isNetworkReachable() {
    //            if hud{
    //                load.show(views: vc.view)
    //            }
                let changeEmailUrl = Constants.APIPath.changeOrderStatus
                let token = self.utility.Retrive(Constants.Strings.Token) as! String
                let headers: HTTPHeaders = [
                                "Content-Type": "application/x-www-form-urlencoded",
                                "Authorization": "Bearer \(token)"
                            ]
               
                Alamofire.request(changeEmailUrl, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: headers).responseData { response in
    //                if hud{
    //                    self.load.hide(delegate: vc)
    //                }
                    switch(response.result){
                    case .success(_):
                        self.printJSON(apiName: changeEmailUrl, data: response.result.value!)
                        print(response.result.description)
                        if response.result.value != nil {
                            completion(response.result.value, "", true)
                        }
                        break
                    case .failure(_):
                        print(response.result.error ?? "Fail")
                        self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                        break
                    }
                }
            }
        }
}
