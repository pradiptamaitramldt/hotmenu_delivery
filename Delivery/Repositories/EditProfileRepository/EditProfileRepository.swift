//
//  EditProfileRepository.swift
//  Delivery
//
//  Created by BrainiumSSD on 10/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class EditProfileRepository: NSObject {
    func editProfile(vc: UIViewController,params : [String : String],completion: @escaping (FetchProfileData, Bool, NSError?) -> Void) {
       
        let request = EditProfileRequest()
        request.editProfileData(vc: vc,parameter  : params, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(FetchProfileData.self, from: response! as! Data)
                    let message = objResponse.message
                    print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
                print("objResponse\(response)")
            }
        }
    }
}
