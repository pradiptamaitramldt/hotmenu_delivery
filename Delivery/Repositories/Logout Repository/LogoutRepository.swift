//
//  LogoutRepository.swift
//  Delivery
//
//  Created by BrainiumSSD on 09/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class LogoutRepository: NSObject {
    
    func logout(vc: UIViewController, completion: @escaping (LogoutResponseModel, Bool, NSError?) -> Void) {
        let request = LogoutRequest()
        request.logout(vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(LogoutResponseModel.self, from: response! as! Data)
                    let message = objResponse.message
                    print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
                print("objResponse\(response)")
            }
        }
    }

}
