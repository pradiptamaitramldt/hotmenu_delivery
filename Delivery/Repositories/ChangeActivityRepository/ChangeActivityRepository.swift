//
//  ChangeActivityRepository.swift
//  Delivery
//
//  Created by BrainiumSSD on 12/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class ChangeActivityRepository: NSObject {
    func changeAcitivityStatus(vc: UIViewController, parameter : [String : Any],completion: @escaping (AlertModel, Bool, NSError?) -> Void) {
        let request = ChangeActivityRequest()
               
        request.changeAcitivityStatus( vc: vc,parameter : parameter ,hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
                   if success{
                       do {
                           let objResponse = try JSONDecoder().decode(AlertModel.self, from: response! as! Data)
                           let message = objResponse.message
                           print("message...\(message ?? "NA")")
                           completion(objResponse, success, nil)
                       } catch let error {
                           print("JSON Parse Error: \(error.localizedDescription)")
                       }
                       print("objResponse\(response)")
                   }
               }
    }

}
