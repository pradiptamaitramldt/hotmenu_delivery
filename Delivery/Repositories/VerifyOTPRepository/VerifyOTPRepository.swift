//
//  VerifyOTPRepository.swift
//  Delivery
//
//  Created by BrainiumSSD on 11/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class VerifyOTPRepository: NSObject {
    func verifyOTP(vc: UIViewController,otpCommand : VerifyOTPCommand, completion: @escaping (VerifyOTPModel, Bool, NSError?) -> Void) {
        let request = VerifyOTPRequest()
        let parameter  = ["email" :otpCommand.email,
        "otp" : otpCommand.OTP] as! [String : String]
        request.verifyOTP(parameter : parameter, vc: vc ,hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(VerifyOTPModel.self, from: response! as! Data)
                    let message = objResponse.message
                    print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
                print("objResponse\(response)")
            }
        }
    }
}
