//
//  AllDeliveriesRepository.swift
//  Delivery
//
//  Created by BrainiumSSD on 12/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class AllOrderRepository : NSObject {

    func getAllOrderList(vc: UIViewController,lat: Double,long : Double, completion: @escaping (OrderListModel, Bool, NSError?) -> Void) {
        let request = AllOrderRequest()
               
        request.getAllOrderList( vc: vc,lat: lat,long: long ,hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
                   if success{
                       do {
                           let objResponse = try JSONDecoder().decode(OrderListModel.self, from: response! as! Data)
                           let message = objResponse.message
                           print("message...\(message ?? "NA")")
                           completion(objResponse, success, nil)
                       } catch let error {
                           print("JSON Parse Error: \(error.localizedDescription)")
                       }
                       print("objResponse\(response)")
                   }
               }
    }
    func getOrderDetails(vc: UIViewController,orderID : String, completion: @escaping (OrderDetailsModel, Bool, NSError?) -> Void) {
        let request = AllOrderRequest()
               
        request.getOrderDetails( vc: vc, orderID: orderID ,hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
                   if success{
                       do {
                           let objResponse = try JSONDecoder().decode(OrderDetailsModel.self, from: response! as! Data)
                           let message = objResponse.message
                           print("message...\(message ?? "NA")")
                           completion(objResponse, success, nil)
                       } catch let error {
                           print("JSON Parse Error: \(error.localizedDescription)")
                       }
                       print("objResponse\(response)")
                   }
               }
    }

    func changeOrderStatus(vc: UIViewController,params : [String : Any], completion: @escaping (AlertModel, Bool, NSError?) -> Void) {
        let request = AllOrderRequest()
               
        request.changeOrderStatus( vc: vc, parameter: params ,hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
                   if success{
                       do {
                           let objResponse = try JSONDecoder().decode(AlertModel.self, from: response! as! Data)
                           let message = objResponse.message
                           print("message...\(message ?? "NA")")
                           completion(objResponse, success, nil)
                       } catch let error {
                           print("JSON Parse Error: \(error.localizedDescription)")
                       }
                       print("objResponse\(response)")
                   }
               }
    }
}
