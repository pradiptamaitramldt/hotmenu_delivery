//
//  JSONResponseDecoder.swift
//  CellularBackupContacts
//
//  Created by Mukesh Singh on 17/10/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation

class JSONResponseDecoder {
    
    typealias JSONDecodeCompletion<T> = (T?, Error?) -> Void
    
    static func decodeFrom<T: Decodable>(_ responseData: Data, returningModelType: T.Type, completion: JSONDecodeCompletion<T>) {
        do {
            let json: [String : Any] = try JSONSerialization.jsonObject(with: Data.init(referencing: responseData as NSData), options: JSONSerialization.ReadingOptions(rawValue: 0)) as! [String : Any]
            Utility.log("String decrypted:\t\t\t\(json)")

        } catch {
            Utility.log("")
        }
        do {
            let model = try JSONDecoder().decode(returningModelType, from: responseData)
            completion(model, nil)
        } catch let DecodingError.dataCorrupted(context) {
            print("Data corrupted: ", context)
            completion(nil, DecodingError.dataCorrupted(context))
        } catch let DecodingError.keyNotFound(key, context) {
            print("Key '\(key)' not found: ", context.debugDescription, "\n codingPath:", context.codingPath)
            completion(nil, DecodingError.keyNotFound(key, context))
        } catch let DecodingError.valueNotFound(value, context) {
            print("Value '\(value)' not found: ", context.debugDescription, "\n codingPath:", context.codingPath)
            completion(nil, DecodingError.valueNotFound(value, context))
        } catch let DecodingError.typeMismatch(type, context) {
            print("Type '\(type)' mismatch: ", context.debugDescription, "\n codingPath:", context.codingPath)
            completion(nil, DecodingError.typeMismatch(type, context))
        } catch {
            Utility.log("error: \(error.localizedDescription)")
            completion(nil, error)
        }
    }
    
}
