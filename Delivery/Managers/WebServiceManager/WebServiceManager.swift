//
//  WebServiceManager.swift
//  CellularBackupContacts
//
//  Created by Mukesh Singh on 17/10/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation

public enum ErrorInfo: String, Error {
    case notConnectedToInternet = "No network Connection"
}

public enum ResponseCode: Int {
    case success = 200
    case userCreateSuccess = 201
    case OTPNotVerified = 403
    case internalDBError = 5005
    case dataAlreadyExist = 2008
    case insufficientInformationProvided = 5002
}

public struct WebServiceConstants {
   
    static private let baseURL = "https://nodeserver.mydevfactory.com:3480/api/"
        
    static var getAllStates: String {
        return self.baseURL + "getAllStates"
    }
    
    static var getAllCities: String {
        return self.baseURL + "getAllCities"
    }
    
    static var registerUser: String {
        return self.baseURL + "customer/register"
    }
    
    static var loginUser: String {
        return self.baseURL + "deliveryBoy/login"
    }
    
    static var forgotPassword: String {
        return self.baseURL + "customer/forgotPassword"
    }
    
    static var resetPassword: String {
        return self.baseURL + "customer/resetPassword"
    }
    
    static var resendOTP: String {
        return self.baseURL + "customer/resendForgotPassOtp"
    }
}

public enum HTTPMethodType: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
}

/// Responsible for generating common headers for requests.
struct WebServiceHeaderGenerator {
    /// Generates common headers specific to APIs. Can also accept additional headers if demanded by specific APIs.
    ///
    /// - Returns: A configured header JSON dictionary which includes both common and additional params.
    public static func generateHeader() -> [String: String] {
        var headerDict = [String: String]()
        headerDict["accept"] = "application/json"
        headerDict["Content-Type"] = "application/json"
        return headerDict
    }
    
    public static func generateHeaderWithPublicKey() -> [String: String] {
        var headerDict = [String: String]()
        let timestamp = NSDate().timeIntervalSince1970
        headerDict["accept"] = "application/json"
        headerDict["public_key"] = "\(WebServiceManager.shared.publicKey)\(Int(timestamp))"
        return headerDict
    }
    public static func generateHeaderWithPublicKeywith(dataStr : String ) -> [String: String] {
        var headerDict = [String: String]()
        let timestamp = NSDate().timeIntervalSince1970
        headerDict["accept"] = "application/json"
        headerDict["public_key"] = "\(WebServiceManager.shared.publicKey)\(Int(timestamp))"
        headerDict["data"] = dataStr
        return headerDict
    }
    
    public static func generateHeaderWithPublicKeyAndAuthTokenwith(dataStr : String ) -> [String: String] {
        var headerDict = [String: String]()
        let timestamp = NSDate().timeIntervalSince1970
        headerDict["accept"] = "application/json"
        headerDict["public_key"] = "\(WebServiceManager.shared.publicKey)\(Int(timestamp))"
        headerDict["Authorization"] = "Bearer \(UserDefaultValues.authToken)"
        headerDict["data"] = dataStr
        return headerDict
    }

    
    public static func generateAuthorizedHeader() -> [String: String] {
        var headerDict = [String: String]()
        headerDict["Content-Type"] = "application/json"
        headerDict["authtoken"] = ""
        //add other fields
        return headerDict
    }
    
}

class WebServiceManager: NSObject {
    
    static let shared = WebServiceManager()
    static let passwordEncription = "Aa7F6d7F9cF0bEb238dBbE0d715E3d6B"
    static let ivCode = "9bAd74D61e7E03bB"
    typealias WebServiceCompletionBlock = (Data?, Error?) -> Void
    var publicKey = ""
    /// Performs a API request which is called by any service request class.
    /// It also performs an additional task of validating the auth token and refreshing if necessary
    ///
    /// - Parameters:
    ///     - apiModel: APIModelType which contains the info about api endpath, header & http method type.
    ///     - completion: Request completion handler.
    
    public func requestAPI(url: String, httpHeader: [String: String]?, parameter: [String: Any]? = nil, httpMethodType: HTTPMethodType, completion: @escaping WebServiceCompletionBlock) {
        if Reachability.isConnectedToNetwork(){
            let escapedAddress = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            var request = URLRequest(url: URL(string: escapedAddress!)!)
            request.httpMethod = httpMethodType.rawValue
            request.allHTTPHeaderFields = httpHeader

            
            Utility.log("URL: \(url)")
            Utility.log("Header: \(httpHeader ?? [:])")
            Utility.log("Parameter: ")
            Utility.log(parameter)
            
            if parameter != nil {
                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: parameter as Any, options: .prettyPrinted)
                } catch let error {
                    Utility.log(error.localizedDescription)
                    return
                }
            }
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    completion(nil, error)
                    return
                }
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    // check for http errors
//                    Utility.log("Error in fetching response")
                    completion(nil, nil)
                }
//                Utility.log("Response: \(String(decoding: data, as: UTF8.self))")
                completion(data, nil)
            }
            task.resume()

        }else{
            completion(nil, ErrorInfo.notConnectedToInternet)
        }
        
    }
    
    //Function with Data
    public func requestAPI(url: String, httpHeader: [String: String], parameter: Data?, httpMethodType: HTTPMethodType, completion: @escaping WebServiceCompletionBlock) {
        if Reachability.isConnectedToNetwork(){
            let escapedAddress = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            var request = URLRequest(url: URL(string: escapedAddress!)!)
            request.httpMethod = httpMethodType.rawValue
            request.allHTTPHeaderFields = httpHeader

            Utility.log("URL: \(url)")
            Utility.log("Header: \(httpHeader)")
            Utility.log("Parameter: ")
            Utility.log(parameter)
            
            if parameter != nil {
                request.httpBody = parameter
            }
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    completion(nil, error)
                    return
                }
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    // check for http errors
//                    Utility.log("Error in fetching response")
                    completion(nil, nil)
                }
//                Utility.log("Response: \(String(decoding: data, as: UTF8.self))")
                completion(data, nil)
            }
            task.resume()

        }else{
            completion(nil, ErrorInfo.notConnectedToInternet)
        }
    }
    
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
//        var strNew = text.replacingOccurrences(of: "/", with: "")
//        strNew = strNew.replacingOccurrences(of: "\", with: "")
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func encryptJsonToString(json : [String: Any]) -> String{
       var encrypted = ""
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: json,
            options: [.prettyPrinted]) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            encrypted = AES256CBC.encryptString(theJSONText!, password: WebServiceManager.passwordEncription)!
        }
        
        return encrypted
    }
    
    func decryptResponseDataToData(data : Data) -> Data?{
        do {
            var json: [String : Any] = try JSONSerialization.jsonObject(with: Data.init(referencing: data as NSData), options: JSONSerialization.ReadingOptions(rawValue: 0)) as! [String : Any]
            
            let decrypted = AES256CBC.decryptString(json["key"] as! String, password: WebServiceManager.passwordEncription)
            Utility.log("String decrypted:\t\t\t\(decrypted!)")
            
            json = self.convertToDictionary(text: decrypted!)!
            do {
                let data = try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
                return data
               } catch let myJSONError {
                   print(myJSONError)
                return nil
               }

        } catch {
            Utility.log("")
            return nil
        }
        
    }
    
    func decryptResponseDataToDataForLogin(data : Data) -> Data?{
        do {
            let json: [String : Any] = try JSONSerialization.jsonObject(with: Data.init(referencing: data as NSData), options: JSONSerialization.ReadingOptions(rawValue: 0)) as! [String : Any]
            
            let decrypted = AES256CBC.decryptString(json["key"] as! String, password: WebServiceManager.passwordEncription)
            Utility.log("String decrypted:\t\t\t\(decrypted!)")
            
            if let data = decrypted!.data(using: .utf8) {
                return data
            }else{
                return nil
            }
            

        } catch {
            Utility.log("")
            return nil
        }
        
    }
    
    func decryptResponseDataToJson(data : Data) -> [String:Any]?{
        do {
            let json: [String : Any] = try JSONSerialization.jsonObject(with: Data.init(referencing: data as NSData), options: JSONSerialization.ReadingOptions(rawValue: 0)) as! [String : Any]
            let decrypted = AES256CBC.decryptString( json ["key"] as! String, password: WebServiceManager.passwordEncription)
            Utility.log("String decrypted:\t\t\t\(decrypted!)")
            
            let objjson = self.convertToDictionary(text: decrypted!)!
            return objjson

        } catch {
            Utility.log("")
            return nil
        }
        
    }
    
    // For Getting the public key
//    func getPublicKey(completion : @escaping (_ success : Bool) -> Void){
//        
//        self.requestAPI(url: WebServiceConstants.getPublicKey, httpHeader: WebServiceHeaderGenerator.generateHeader(), httpMethodType: .get) { (data, err) in
//            
//            if let data = data {
//                
//                do {
//                    var json: [String : Any] = try JSONSerialization.jsonObject(with: Data.init(referencing: data as NSData), options: JSONSerialization.ReadingOptions(rawValue: 0)) as! [String : Any]
//                    
//                    let decrypted = AES256CBC.decryptString( json ["key"] as! String, password: WebServiceManager.passwordEncription)
//                    Utility.log("String decrypted:\t\t\t\(decrypted!)")
//                    
//                    json = self.convertToDictionary(text: decrypted!)!
//                    Utility.log(json)
//                    if let key = json["public_key"] as! String?{
//                        self.publicKey = key
//                        completion(true)
//                    }
//                    else{
//                        Utility.log("Key not found")
//                        completion(false)
//                    }
//                } catch {
//                    Utility.log("")
//                    completion(false)
//                }
//            }
//
//        }
//          
//      }
    
    
}

