//
//  OrderListModel.swift
//  Delivery
//
//  Created by BrainiumSSD on 12/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation

// MARK: - OrderListModel
struct OrderListModel: Codable {
    let success: Bool
    let statuscode: Int
    let message: String
    let responseData: [OrderListData]

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseDatum
struct OrderListData: Codable {
    let deliveryStatus: Int
    let id: String
    let orderID: OrderID
    let vendorID: VendorID
    let deliveryBoyID: String
    let customerID: CustomerID
    let createdAt, updatedAt: String
    let v: Int
    let deliveryAddressID: DeliveryAddressID
    let restaurantDistanceFromDeliveryBoy, deliveryLocationDistanceFromRestaurant: String
    let vendorLatitude, vendorLongitude, deliveryLatitude, deliveryLongitude: Double

    enum CodingKeys: String, CodingKey {
        case deliveryStatus
        case id = "_id"
        case orderID = "orderId"
        case vendorID = "vendorId"
        case deliveryBoyID = "deliveryBoyId"
        case customerID = "customerId"
        case createdAt, updatedAt
        case v = "__v"
        case deliveryAddressID = "deliveryAddressId"
        case restaurantDistanceFromDeliveryBoy, deliveryLocationDistanceFromRestaurant, vendorLatitude, vendorLongitude, deliveryLatitude, deliveryLongitude
    }
}
// MARK: - DeliveryAddressID
struct DeliveryAddressID: Codable {
    let location: Location
    let isDefault: Bool
    let landmark, id, addressType, fullAddress: String
    let flatOrHouseOrBuildingOrCompany, userID, createdAt, updatedAt: String
    let v: Int

    enum CodingKeys: String, CodingKey {
        case location, isDefault, landmark
        case id = "_id"
        case addressType, fullAddress, flatOrHouseOrBuildingOrCompany
        case userID = "userId"
        case createdAt, updatedAt
        case v = "__v"
    }
}

// MARK: - Location
struct Location: Codable {
    let coordinates: [Double]
    let type: String
}
// MARK: - CustomerID
struct CustomerID: Codable {
    let email, countryCode: String
    let phone: Int
    let profileImage, id: String
    let fullName : String?
    enum CodingKeys: String, CodingKey {
        case email, countryCode, phone, profileImage,fullName
        case id = "_id"
    }
}

// MARK: - OrderID
struct OrderID: Codable {
    let orderPrepTime, id, orderNo: String
    let cartID: CartID
    let finalAmount: Int

    enum CodingKeys: String, CodingKey {
        case orderPrepTime
        case id = "_id"
        case orderNo
        case cartID = "cartId"
        case finalAmount
    }
}

// MARK: - CartID
struct CartID: Codable {
    let menus: [Menu]
}

// MARK: - Menu
struct Menu: Codable {
    let menuAddedDate, id: String
    let menuID: MenuID
    let menuAmount, menuQuantity, menuTotal: Int

    enum CodingKeys: String, CodingKey {
        case menuAddedDate
        case id = "_id"
        case menuID = "menuId"
        case menuAmount, menuQuantity, menuTotal
    }
}

// MARK: - MenuID
struct MenuID: Codable {
    let itemID: ItemID

    enum CodingKeys: String, CodingKey {
        case itemID = "itemId"
    }
}

// MARK: - ItemID
struct ItemID: Codable {
    let itemName, type, menuImage: String
}

// MARK: - VendorID
struct VendorID: Codable {
    let location: Location
    let id, restaurantName: String
    let contactPhone: Int

    enum CodingKeys: String, CodingKey {
        case location
        case id = "_id"
        case restaurantName, contactPhone
    }
}

