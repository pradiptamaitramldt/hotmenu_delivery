//
//  OrderDetailsModel.swift
//  Delivery
//
//  Created by BrainiumSSD on 12/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation

// MARK: - OrderDetailsModel
struct OrderDetailsModel: Codable {
    let success: Bool
    let statuscode: Int
    let message: String
    let responseData: OrderDetailsData

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct OrderDetailsData: Codable {
    let deliveryStatus: Int
    let id: String
    let orderID: OrderID
    let vendorID: VendorID
    let deliveryBoyID: String
    let customerID: CustomerID
    let createdAt, updatedAt: String
    let v: Int
    let deliveryAddressID: DeliveryAddressID
    let vendorLatitude, vendorLongitude, deliveryLatitude, deliveryLongitude: Double
    
    enum CodingKeys: String, CodingKey {
        case deliveryStatus
        case id = "_id"
        case orderID = "orderId"
        case vendorID = "vendorId"
        case deliveryBoyID = "deliveryBoyId"
        case customerID = "customerId"
        case createdAt, updatedAt
        case v = "__v"
        case deliveryAddressID = "deliveryAddressId"
        case vendorLatitude, vendorLongitude, deliveryLatitude, deliveryLongitude
    }
}

