//
//  AlertModel.swift
//  Delivery
//
//  Created by BrainiumSSD on 12/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation
// MARK: - AlertModel
struct AlertModel: Codable {
    let success: Bool
    let statuscode: Int
    let message: String

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
    }
}
