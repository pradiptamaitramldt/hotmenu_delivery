//
//  VerifyOTPModel.swift
//  Delivery
//
//  Created by BrainiumSSD on 11/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation
// MARK: - VerifyOTPModel
struct VerifyOTPModel: Codable {
    let success: Bool
    let statuscode: Int
    let message: String
   // let responseData: VerifiedData

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
      //  case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct VerifiedData: Codable {
    let id, userID, email: String
    let otp: Int
    let usedFor: String
    let status: Int
    let createdAt, updatedAt: String
    let v: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case userID = "userId"
        case email, otp, usedFor, status, createdAt, updatedAt
        case v = "__v"
    }
}

