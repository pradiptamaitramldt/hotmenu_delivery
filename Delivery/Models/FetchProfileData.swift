//
//  FetchProfileData.swift
//  Delivery
//
//  Created by BrainiumSSD on 10/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation

// MARK: - FetchProfileData
struct FetchProfileData: Codable {
    let success: Bool
    let statuscode: Int
    let message: String
    let responseData: ProfileData

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct ProfileData: Codable {
    let firstName, lastName, email, countryCode: String
    let phone: Int
    let vehicle, numberPlate, driverLicense, profileImage: String
    let isActive : Bool
}
