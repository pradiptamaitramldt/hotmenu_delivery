//
//  LogoutModel.swift
//  Delivery
//
//  Created by BrainiumSSD on 09/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation
// MARK: - LogoutResponseModel
class LogoutResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
   // let responseData: RegistraionModel?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
       // case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
      //  self.responseData = responseData
    }
}
