//
//  NotificationListCell.swift
//  EazzyEatsDelivery
//
//  Created by Ivica Technologies on 05/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class NotificationListCell: UITableViewCell {
    
    
    @IBOutlet weak var DiscLabel: CustomNormalLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
