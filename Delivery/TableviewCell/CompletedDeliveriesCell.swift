//
//  CompletedDeliveriesCell.swift
//  EazzyEatsDelivery
//
//  Created by Ivica Technologies on 10/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class CompletedDeliveriesCell: UITableViewCell {

    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var viewDetailesButton: UIButton!
    @IBOutlet var customerNameLabel: CustomBoldLabel!
    @IBOutlet var restaurantNameLabel: CustomBoldLabel!
    @IBOutlet var dateLabel: CustomNormalLabel!
    @IBOutlet var timeLabel: CustomNormalLabel!
    
    @IBOutlet var labelType: CustomNormalLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
