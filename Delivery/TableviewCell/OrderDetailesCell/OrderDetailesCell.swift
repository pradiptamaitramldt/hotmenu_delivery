//
//  OrderDetailesCell.swift
//  EazzyEatsDelivery
//
//  Created by Ivica Technologies on 13/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class OrderDetailesCell: UITableViewCell {

    @IBOutlet weak var itemName: UILabel!
    @IBOutlet var itemImage: UIImageView!
    @IBOutlet var itemQurnatityLabel: CustomNormalLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
